.\" This file is part of Vcsync -*- nroff -*-
.\" Copyright (C) 2014-2024 Sergey Poznyakoff
.\"
.\" Vcsync is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Vcsync is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
.TH VCSYNC 8 "March 13, 2024" "VCSYNC"
.SH NAME
vcsync \- synchronize version control system updates with the file system
.SH SYNOPSIS
.nh
.na
\fBvcsync\fR [\fB\-bdent\fR] [\fB\-D\fR \fIDIR\fR] [\fB\-N\fR \fISTRING\fR]\
 [\fB\-c\fR \fIFILE\fR] [\fB\-l\fR \fIPRIO\fR]\
 [\fB\-s\fR \fISECONDS\fR]\
 [\fB\-T\fR \fITYPE\fR]\
 [\fB\-w\fR \fIDIR\fR]\
 [\fB\-\-detach\fR]\
 [\fB\-\-config=\fIFILE\fR] [\fB\-\-debug\fR]\
 [\fB\-\-destination\-root=\fIDIR\fR] [\fB\-\-checkoutrc=\fIDIR\fR]\
 [\fB\-\-dry\-run\fR] [\fB\-\-lint\fR] [\fB\-\-name=\fISTRING\fR]\
 [\fB\-\-sleep=\fISECONDS\fR]\
 [\fB\-\-stderr\fR] [\fB\-\-vcs\-type=\fITYPE\fR]\
 [\fIARG\fR...]
.PP
\fBvcsync\fR [\fB\-HhV\fR] [\fB\-\-usage\fR] [\fB\-\-version\fR]\
 [\fB\-\-help\fR] [\fB\-\-config\-help\fR]
.ad
.hy
.SH DESCRIPTION
It is often necessary to maintain a directory tree which is an exact
copy of the data stored in a version control system repository.
\fBVcsync\fR aims to help solve this problem for 
.BR CVS ,
.BR SVN ", and"
.BR GIT .
.PP
The synopsis is as follows.  There is a version control system
repository, whose pathname is \fIREPO_ROOT\fB/\fIPROJECT\fR and a
directory \fIDEST_ROOT\fB/\fIPROJECT\fR, called destination directory.
The task is to ensure that each check in to the repository be
immediately reflected in the destination directory.
.PP
To solve that task, \fBvcsync\fR is installed as a hook that is
invoked upon each modification of the repository.  It takes the
information about the VCS in use, repository and destination
paths either from the command line options or from its configuration
file.  The details differ depending on the flavor of VCS used and
are described below.
.SS CVS
When used with \fBCVS\fR, the program is invoked from
.B CVSROOT/loginfo
as follows:
.PP
.RS
.EX
ALL /usr/bin/vcsync --vcs-type=cvs --detach --sleep=2 -- %p %s
.EE
.RE
.PP
The \fB\-\-vcs-type\fR option tells \fBvcsync\fR which \fBVCS\fR to use.
The \fB\-\-detach\fR and \fB\-\-sleep\fR options instruct it to detach from the
controlling terminal right after start up and wait for two seconds before
performing its job.  These two options avoid dead-locking condition which
occurs when \fBcvs update\fR is invoked from the \fBloginfo\fR file.
.PP
The two dashes mark end of options.  This is a precaution for cases
when expansion of \fB%p\fR begins with a dash.
.PP
If using old CVS format strings, the following line should be used:
.PP
.RS
.EX
ALL /usr/bin/vcsync --vcs-type=cvs --detach --sleep=2 -- %s
.EE
.RE
.PP
Such usage, however, is not recommended.
.PP
The options can be stored in the configuration file (see \fBCONFIGURATION\fR
below).  The simplest configuration file will look like:
.PP
.RS
.EX
# Define the VCS type.
vcs-type cvs;
# Detach from the controlling terminal.
detach yes;
# Wait for the check in process to terminate.
sleep 2;
# Destination directory is located in this directory.
destination-root /srv/home;
.EE
.RE
.PP
Then the line in \fBCVSROOT/loginfo\fR can be simplified to:
.PP
.RS
.EX
ALL /bin/vcsync -- %p %s
.EE
.RE
.PP
The \fBCVS\fR synchronization routine gets the name of the project (the
\fIPROJECT\fR part in the discussion above) from the value of the 
environment variable
.BR CVSROOT ,
by selecting its trailing directory component.  For example, given the
configuration file above and
.nh
.BR CVSROOT=/var/cvsroot/foo ,
.hy
the destination directory will be located in
.nh
.BR /srv/home/foo .
.hy
.SS SVN
The utility must be configured as a \fBpost-commit\fR
hook.  It expects the same arguments as the hook, i.e. path to the
repository as the first argument, and revision string as the second
one.  Given the proper configuration file, it can be set up as:
.PP
.RS
.EX
$ cd repo/hooks
$ ln -sf /usr/bin/vcsync post-commit
.EE
.RE
.PP
The configuration file should then contain at least:
.PP
.RS
.EX
vcs-type svn;
destination-root /srv/home;
.EE
.RE
.PP
The \fBvcs\-type\fR statement is optional.  If missing, it will be
detected automatically.  See the section \fBVCS DETECTION\fR for details.
.PP
The project name is deduced from the first argument, by selecting its
trailing directory component.
.SS GIT
.B Vcsync
must be invoked as git
.B post-receive
hook.  For example:
.PP
.RS
.EX
$ cd repo.git/hooks
$ ln -s /usr/bin/vcsync post-receive
.EE
.RE
.PP
The simplest configuration file is:
.PP
.RS
.EX
vcs-type git;
destination-root /srv/home;
.EE
.RE
.PP
The \fBvcs\-type\fR statement is optional.  If missing, it will be
detected automatically.  See the section \fBVCS DETECTION\fR for details.
.PP
The project name is obtained from the last directory component of the
current working directory (which is set by \fBgit\fR to the repository
being modified).  The \fB.git\fR suffix will be removed.
.SH OPTIONS
.SS Operation mode
The options below are designed for testing and debugging purposes.
They select a specific operation mode, and disable the usual
\fBvcsync\fR behavior. Only one of them can be used in the command line.
.PP
.TP
\fB\-D\fR, \fB\-\-checkoutrc=\fIDIR\fR
Run \fB.checkoutrc\fR file in directory \fIDIR\fR.  If \fIDIR\fR
ends with a slash, descend recursively into its subdirectories and
process \fB.checkoutrc\fR files found there.  See the section
\fBCHECKOUTRC\fR below for a detailed discussion of \fB.checkoutrc\fR
files and their purpose.
.TP
\fB\-t\fR, \fB\-\-lint\fR
Test configuration file syntax and exit.
.SS Various settings
These options modify various configuration settings.  They are
equivalent to the eponymous configuration file statements and override
these if present.
.PP
.TP
\fB\-T\fR, \fB\-\-vcs\-type=\fBcvs\fR|\fBsvn\fR|\fBgit\fR
Sets VCS type.
.TP
\fB\-b\fR, \fB\-\-detach\fR
Detach from controlling terminal after startup.  This is mostly for
use with \fBCVS\fR (together with the \fB\-s\fR option).
.TP
\fB\-s\fR, \fB\-\-sleep=\fINUMBER\fR
Sleep \fINUMBER\fR of seconds before doing the job.
.TP
\fB\-w\fR, \fB\-\-destination\-root=\fIDIR\fR
Defines destination root directory.
.SS Logging and debugging
By default \fBvcsync\fR logs errors to the syslog facility
\fBuser\fR.  The options below modify this behavior.
.PP
.TP
\fB\-d\fR, \fB\-\-debug\fR
Increase debug level.
.TP
\fB\-e\fR, \fB\-\-stderr\fR
Log everything to standard error instead of the syslog.
.TP
\fB\-l\fR \fIPRIO\fR
Log everything with priority \fIPRIO\fR and higher to the stderr, as
well as to the syslog.
.TP
\fB\-n\fR, \fB\-\-dry\-run\fR
Do nothing, only print what would have been done.
.SS Selecting configuration
The settings can be read from a configuration file (normally
.nh
.BR /etc/vcsync.conf ,
.hy
see the section
.B CONFIGURATION
for a detailed description of it).  Two options are available to
select the configuration settings to use:
.TP
\fB\-c\fR, \fB\-\-config=\fIFILE\fR
Read configuration from \fIFILE\fR instead of the default location.
.TP
\fB\-N\fR, \fB\-\-name=\fISTRING\fR
The configuration file can contain several sets of settings, called
.B "named configurations" 
(see the description of the \fBconfig\fR statement in the
\fBCONFIGURATION\fR section).  This makes it possible to have single
configuration file for use with different version control systems.
The \fB\-N\fR option is used to select the set to use.
.SS Informational options
.TP
\fB\-V\fR, \fB\-\-version\fR
Print program version.
.TP
\fB\-h\fR, \fB\-\-help\fR
Print a short help summary.
.TP
\fB\-\-usage\fR
Print a terse summary of the command line syntax.
.TP
\fB\-H\fR, \fB\-\-config\-help\fR
Display help on the configuration file syntax.
.SH CONFIGURATION
The program reads its configuration from the file
.nh
.B /etc/vcsync.conf
.hy
(the exact location can be changed at compilation time).  Since most of
the settings can be given in the command line, this file is optional
and it is not considered an error if it does not exist (the warning
about the fact is displayed if \fBvcsync\fR is run with the debugging
level 1 or higher, though).  An alternative configuration file can be
specified using the \fB\-c\fR (\fB\-\-config\fR) command line option.
.PP
For a detailed description of the configuration file syntax and
available statements, refer to
.BR vcsync.conf (5).
The discussion below is but a concise summary of its statements.
.TP
.BI "debug " number ;
Sets the debug level.
.TP
\fBvcs\-type\fR \fBcvs\fR|\fBsvn\fR|\fBgit;\fR
Sets the type of VCS to use.  The default is \fBcvs\fR.
.TP
.BI "vcs\-command " string ;
Use \fIstring\fR instead of the default \fBVCS\fR command.  The
default commands are:
.BR /usr/bin/cvs ,
.BR /usr/bin/svn ,
.BR /usr/bin/git .
.TP
.BI "destination-root " string ;
Sets the location of the destination root directory.
.TP
.BI "timeout " number ;
Sets execution timeout for a single \fBVCS\fR command, in seconds.
The default is 5 seconds.
.TP
.BI "detach " boolean ;
Detach from the controlling terminal before synchronizing.  The
\fBdetach true\fR statement should be used with \fBcvs\fR, together
with the \fBsleep\fR statement (see below).
.TP
.BI "sleep " number ;
Wait \fInumber\fR of seconds before synchronizing.  This is necessary
for \fBcvs\fR, to avoid dead-locking.
.TP
.BI "message " string
Output this message to stdout before synchronizing.  Notice that it
depends on the \fBVCS\fR in use whether this text will be visible to
the remote user or not.  It works with \fBCVS\fR and \fBGIT\fR, but
does not with \fBSVN\fR.
.SS Named configurations
A named configuration is introduced by the following statement:
.PP
.RS
.EX
config \fIname\fR {
    \fIstatements\fR
}
.EE
.RE
.PP
where \fIstatements\fR is a list of statements discussed above.  This
configuration takes effect (instead of the global configuration), if
\fBvcsync\fR is invoked with the \fB\-N\fR (\fB\-\-name\fR) command line
option whose argument matches the \fIname\fR in the \fBconfig\fR
block.  Named configurations are also used if the VCS type is not
explicitly specified.  See the section \fBVCS DETECTION\fR for details.
.SS Logging
The following configuration statement controls the \fBsyslog\fR output:
.sp
.nf
.in +2
.B syslog {
.in +4
.fi
\fBfacility \fBuser\fR|\fBdaemon\fR|\fBauth\fR|\fBauthpriv\fR|\fBmail\fR|\fBcron\fR|\fBlocal0-local7\fR;\fR
.nf
.BI "tag " STRING ;
.BI "print\-priority " BOOL ;
.in -4
.B }
.in
.fi
.SH VCS DETECTION
If the VCS type is not supplied explicitly (by the \fBvcs\-type\fR configuration
setting or the \fB\-\-vcs\-type\fR option), the program attempts to
determine it by looking at the name with which it is invoked.  It is
able to correctly determine the VCS type, if it is being run from the
\fBhooks\fR subdirectory of a \fBgit\fR or \fBSVN\fR repository.
Otherwise, it will assume the default VCS type.
.PP
When the VCS type is established, the program will look in the
configuration file for the first named configuration that lists the
detected type in its \fBvcs\-type\fR statement and will use it.  If no
such configuration is found, the default one will be used.
.PP
Thus, a single configuration file can be used for all three supported
version control systems.
.SH CHECKOUTRC
A special feature is provided to use
.B vcsync
as a deployment engine for web pages.
After synchronizing destination directory with the repository, it
scans each updated directory for the presence of files named
.B .htaccess
and
.BR .checkoutrc .
If
.B .htaccess
is found, it is removed.  If
.B .checkoutrc
is found, it is read and processed line by line according to the
following rules:
.nr step 1 1
.IP \n[step].
The
.B #
character introduces a comment.  It and anything after it up to the nearest
newline is removed.
.IP \n+[step].
Empty lines are ignored.
.IP \n+[step].
Non-empty lines must contain one of the following statements.  The
keywords are case-insensitive:
.PP
.TP
\fBlink\fR \fIsrc\fR \fIdst\fR
Create a symbolic link from \fIsrc\fR to \fIdst\fR.  Neither argument
can be an absolute pathname or contain references to the parent
directory.
.TP
\fBunlink\fR \fIfile\fR
Unlink the file.  The argument must refer to a file in the current
directory or one of its subdirectories, that is either a symbolic link
to another file or 
.B .htaccess
file.
.TP
\fBchmod\fR \fImode\fR \fIfile\fR [\fIfile\fR...]
Change mode of the listed files to \fImode\fR, which must be an octal
number.  Shell wildcards are allowed in \fIfile\fR arguments.  After
expandind, each file argument must refer to a file in the current
directory or one of its subdirectories.
.PP
The administrator can also define additional keywords, as described in
.BR vcsync.conf (5),
subsection
.BR "Checkoutrc keywords" .
Appearance of these user-defined keywords in the \fB.checkoutrc\fR
file cause creation and modification of the
.B .htaccess
file in the current directory.
.PP
The default configuration file shipped with the distribution defines
the following keywords:
.TP
\fBxbithack\fR \fBon\fR|\fBoff\fR
If set to \fBon\fR, adds the following to 
.BR .htaccess :
.RS
.EX
Options +IncludesNOEXEC
XBitHack On
.EE
.RE
.TP
\fBssi\fR \fBon\fR|\fBoff\fR
If set to \fBon\fR, enables server-side includes, by adding the 
following statement to the \fB.htaccess\fR file: 
.RS
.EX
AddHandler server-parsed .html
.EE
.RE
.PP
The following statements are reproduced in
.B .htaccess
verbatim:
.PP
.nh
.na
\fBredirect\fR \fIargs\fR...
.br
\fBrewritebase\fR \fIargs\fR...
.br
\fBrewritecond\fR \fIargs\fR...
.br
\fBrewriteengine\fR \fIargs\fR...
.br
\fBrewritemap\fR \fIargs\fR...
.br
\fBrewriteoptions\fR \fIargs\fR...
.br
\fBrewriterule\fR \fIargs\fR...
.ad
.hy
.SH NOTE
This manpage is a short description of \fBvcsync\fR.  For a detailed
discussion, including examples and usage recommendations, refer to the
\fBVcsync User Manual\fR available in texinfo format.  If the \fBinfo\fR
reader and the \fBvcsync\fR documentation are properly installed on your
system, the command
.PP
.RS +4
.B info vcsync
.RE
.PP
should give you access to the complete manual.
.PP
You can also view the manual using the info mode in
.BR emacs (1),
or find it in various formats online at
.PP
.RS +4
.B http://www.gnu.org.ua/software/vcsync
.RE
.PP
If any discrepancies occur between this manpage and the
\fBVcsync User Manual\fR, the later shall be considered the authoritative
source.
.SH "EXIT CODE"
.IP 0
Successful termination.
.IP 1
Failure during synchronization.
.IP 2
Command line usage error.
.IP 3
Configuration file syntax error.
.\" The MANCGI variable is set by man.cgi script on Ulysses.
.\" The download.inc file contains the default DOWNLOAD section
.\" for man-based doc pages.
.if "\V[MANCGI]"WEBDOC" \{\
.       ds package vcsync
.       ds version 1.1
.       so download.inc
\}
.SH "SEE ALSO"
.BR vcsync.conf (5),
.BR cvs (1),
.BR svn (1),
.BR git (1),
.BR githooks (5).
.SH AUTHORS
Sergey Poznyakoff <gray@gnu.org>
.SH "BUG REPORTS"
Report bugs to <bug-vcsync@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2014-2024 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

