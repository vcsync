.\" This file is part of Vcsync -*- nroff -*-
.\" Copyright (C) 2014-2024 Sergey Poznyakoff
.\"
.\" Vcsync is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Vcsync is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
.TH VCSYNC.CONF 5 "March 13, 2024" "VCSYNC.CONF"
.SH NAME
vcsync.conf \- configuration file for vcsync
.SH DESCRIPTION
The file consists of statements terminated with a semicolon.
White-space characters (space, horizontal and vertical tabulation,
newline, form-feed) are ignored, except as they serve to separate the
tokens.  Empty lines and comments are ignored.  There are two kinds of
comments: single-line and multi-line.  Single-line comments start
with
.B #
or
.B //
and continue to the end of the line:
.sp
.RS 4
.nf
# This is a comment
// This too is a comment
.fi
.RE
.PP
\fIMulti-line\fB or \fIC-style\fR comments start with the two
characters
.B /*
(slash, star) and continue until the first occurrence of
.B */
(star, slash).
.PP
Multi-line comments cannot be nested.  However, single-line comments
may well appear within multi-line ones.
.PP
Pragmatic comments are similar to the usual single-line comments,
except that they cause some changes in the way the configuration is
parsed.  Pragmatic comments begin with a
.B #
sign and end with the next physical newline character.
.TP
.BI "#include <" "FILE" >
.PD 0
.TP
.BI "#include " "FILE"
.PD
Include the contents of the file \fIfile\fR.  Both forms are
equivalent.  The \fIFILE\fR must be an absolute file name.
.TP
.BI "#include_once <" "FILE" >
.PD 0
.TP
.BR "#include_once " "FILE"
.PD
Same as \fB#include\fR, except that, if the \fIFILE\fR has already
been included, it will not be included again.
.TP
.BI "#line " "num"
.PD 0
.TP
.BI "#line " "num" " \(dq" "FILE" "\(dq"
.PD
This line causes the parser to believe, for purposes of error
diagnostics, that the line number of the next source line
is given by \fInum\fR and the current input file is named by
\fIFILE\fR. If the latter is absent, the remembered file name
does not change.
.TP
.BI "# " "num" " \(dq" "FILE" "\(dq"
This is a special form of the \fB#line\fR statement, understood for
compatibility with the C preprocessor.
.SS Statements
A statement consists of a keyword and the value, separated with any
amount of whitespace.  Statements are terminated with a semicolon.
A \fIvalue\fR can be one of the following:
.TP
.I number
A number is a sequence of decimal digits.
.TP
.I boolean
A boolean value is one of the following: \fByes\fR, \fBtrue\fR,
\fBt\fR or \fB1\fR, meaning \fItrue\fR, and \fBno\fR,
\fBfalse\fR, \fBnil\fR, \fB0\fR meaning \fIfalse\fR.
.TP
.I unquoted string
An unquoted string may contain letters, digits, and any of the
following characters: \fB_\fR, \fB\-\fR, \fB.\fR, \fB/\fR,
\fB@\fR, \fB*\fR, \fB:\fR.
.TP
.I quoted string
A quoted string is any sequence of characters enclosed in
double-quotes (\fB\(dq\fR).  A backslash appearing within a quoted
string introduces an \fIescape sequence\fR, which is replaced
with a single character according to the following rules:
.sp
.nf
.ta 8n 18n 42n
.ul
	Sequence	Expansion	ASCII
	\fB\\\\\fR	\fB\\\fR	134
	\fB\\"\fR	\fB"\fR	042
	\fB\\a\fR	audible bell	007	
	\fB\\b\fR	backspace	010
	\fB\\f\fR	form-feed	014
	\fB\\n\fR	new line	012
	\fB\\r\fR	carriage return	015
	\fB\\t\fR	horizontal tabulation	011
	\fB\\v\fR	vertical tabulation	013
.fi

In addition, the sequence \fB\\\fInewline\fR is removed from
the string.  This allows to split long strings over several
physical lines, e.g.:
.sp
.nf
.in +4
"a long string may be\\
 split over several lines"
.in
.fi
.sp
If the character following a backslash is not one of those specified
above, the backslash is ignored and a warning is issued.

Two or more adjacent quoted strings are concatenated, which gives
another way to split long strings over several lines to improve
readability.  The following fragment produces the same result as the
example above:
.sp
.nf
.in +4
"a long string may be"
" split over several lines"
.in
.fi
.TP
.I Here-document
A \fIhere-document\fR is a special construct that allows to introduce
strings of text containing embedded newlines.  

The
.BI "<<" "word"
construct instructs the parser to read all the following lines up to
the line containing only \fIword\fR, with possible trailing blanks.
Any lines thus read are concatenated together into a single string.
For example: 
.sp
.nf
.in +4
<<EOT
A multiline
string
EOT
.in
.fi
.sp
The body of a here-document is interpreted the same way as a
double\-quoted string, unless \fIword\fR is preceded by a backslash
(e.g.  \fB<<\\EOT\fR) or enclosed in double\-quotes, in which case
the text is read as is, without interpretation of escape sequences.

If \fIword\fR is prefixed with \fB\-\fR (a dash), then all leading
tab characters are stripped from input lines and the line containing
\fIword\fR.  Furthermore, \fB\-\fR is followed by a single space,
all leading whitespace is stripped from them.  This allows to indent
here-documents in a natural fashion.  For example:
.sp
.nf
.in +4
<<\- TEXT
    The leading whitespace will be
    ignored when reading these lines.
TEXT
.in
.fi
.sp
It is important that the terminating delimiter be the only token on
its line.  The only exception to this rule is allowed if a
here-document appears as the last element of a statement.  In this
case a semicolon can be placed on the same line with its terminating 
delimiter, as in: 
.sp
.nf
.in +5
message <<\-EOT
    Synchronizing project directory.
    Please wait.
EOT;
.in
.fi
.SS Block statements
These are special statements that have as their value a list of
another statements enclosed in curly braces.  The terminating
semicolon is optional.
.SH STATEMENTS
.SS Synchronization settings
.TP
.BI "debug " number ;
Sets the debug level.  See the description of meaningful levels in
.BR vcsync (8).
.TP
\fBvcs\-type\fR \fBcvs\fR|\fBsvn\fR|\fBgit;\fR
Sets the type of VCS to use.  The default is \fBcvs\fR.
.TP
.BI "vcs\-command " string ;
Use \fIstring\fR instead of the default \fBVCS\fR command.  The
default commands are:
.BR /usr/bin/cvs ,
.BR /usr/bin/svn ,
.BR /usr/bin/git .
.TP
.BI "destination-root " string ;
Sets the location of the destination root directory.
.TP
.BI "timeout " number ;
Sets command execution timeout in seconds.  If the \fBVCS\fR command
takes longer than
.I number
seconds to execute, it will be forcibly terminated.  The default
timeout is 5 seconds.
.TP
.BI "detach " boolean ;
Detach from the controlling terminal before synchronizing.  The
\fBdetach true\fR statement should be used with \fBcvs\fR, together
with the \fBsleep\fR statement (see below).
.TP
.BI "sleep " number ;
Wait \fInumber\fR of seconds before synchronizing.  This is necessary
for \fBcvs\fR, to avoid dead-locking.
.TP
.BI "message " string
Output this message to stdout before synchronizing.  Notice that it
depends on the \fBVCS\fR in use whether this text will be visible to
the remote user or not.  It works with \fBCVS\fR and \fBGIT\fR, but
does not with \fBSVN\fR.
.SS Foreign destinations
A destination directory is called
.I foreign
if it was not created
as a result of checkout from the VCS that triggered its update.
Such foreign destinations can occur, for example, if CVS was initially
used as the version control system, but then it got replaced by git.
.PP
The behavior of the program upon encountering such directories is
controlled by following configuration settings:
.TP
.BI "foreign\-dir\-mode " mode
Defines the action to be taken upon encountering a foreign
destination.  The
.I mode
argument can be one of the following:
.RS
.TP
.B error
Issue error diagnostic message and abort the syncronization.  This is
the default behavior.
.TP
.B remove
Remove the foreign directory and continue with a clean checkout from
the VCS.
.TP
.B rename
Rename the foregn directory and continue with a clean checkout from
the VCS.
.IP
The new name is constructed as \fIdirname\fB\-\fIuniq\fB.vcsync\fR, where
.I dirname
is the original directory name, and
.I uniq
is a 6-character sequence of alphanumeric characters selected so as to make
the name unique within the directory.
.TP
.B archive
Create an archive of the foreign directory, then remove the directory,
and proceed with the checkout.  The archive is created using an
external command defined by the
.B archive\-command
configuration statement.
.RE
.TP
.BI "archive-command " command
External command used if
.B foreign\-dir\-mode
is set
.IR archive .
The
.B $DIRNAME
variable will be expanded to the name of the destination directory.
No other expansions take place.
.SS Sentinel file
A \fIsentinel\fR is a special file name which, when present in the
destination directory, tells \fBvcsync\fR that this directory is
exempt from synchronization.
.TP
.BI "sentinel " filename
Declares the sentinel file name. The \fIfilename\fR must be a valid
file name, without directory prefix.
.SS Named configuration
The settings described above set up a global configuration.  The
configuration file can also contain any number of
.BR "named configurations" ,
which have the following syntax:
.PP
.RS
.EX
config \fIname\fR {
    \fIstatements\fR
}
.EE
.RE
.PP
where \fIstatements\fR is a list of statements discussed above.  These
settings are ignored, unless \fBvcsync\fR is invoked with the
\fB\-N\fR (\fB\-\-name) command line option.  In that case, the option
gives the \fIname\fR  of the \fBconfig\fR block to use instead of the
default settings.  For example:
.PP
.RS
.EX
vcs-type cvs;
destination-root /var/cvsroot;
detach yes;
sleep 1;

config alt {
    vcs-type svn;
    destination-root /var/svnroot;
}
.EE
.RE
.PP
Then, to use the \fBsvn\fR configuration, \fBvcsync\fR should be run
as follows:
.PP
.RS
.EX
vcsync --name alt [\fIother arguments\fR]
.EE
.RE
.SS Access file
.TP
\fBaccess\-file\-name\fR \fIname\fB;\fR
Create files \fIname\fR, instead of the default \fB.htaccess\fR.
.TP
\fBaccess\-file\-text\fR \fItext\fB;\fR
Add \fItext\fR to the beginning of each created access file.
.SS Checkoutrc keywords
The syntax of \fB.checkoutrc\fR files can be expanded with new keywords,
if the need be.  There are two kinds of keywords: \fBverbatim\fR
and \fBboolean\fR.  Verbatim keywords cause the entire line from
\fB.checkoutrc\fR to be reproduced in \fB.htaccess\fR.  Boolean
ones take a boolean value as their argument.  If that value evaluates
to true, a text assigned to the keyword is inserted in the current
position of \fB.htaccess\fR.  Otherwise, the keyword is ignored.
.PP
You can use the \fBdefine\fR statement to define new \fB.checkoutrc\fR
keywords.  Please do so sparingly and consider possible security
implications.
.TP
\fBdefine\fR \fIkeyword\fB;\fR
Define a verbatim keyword.
.TP
\fBdefine\fR \fIkeyword\fR \fItext\fB;\fR
Define a boolean keyword.
.SS Logging
The following configuration statement controls the \fBsyslog\fR output:
.sp
.nf
.in +2
.B syslog {
.in +4
.BI "facility " string ;
.BI "tag " string ;
.BI "print\-priority " bool ;
.in -4
.B }
.in
.fi
.PP
The statements are:
.TP
.BI "facility " string ;
Set \fBsyslog\fR facility.  Valid argument values are:
.BR user ,
.BR daemon ,
.BR auth " or " authpriv ,
.BR mail ,
.BR cron ,
.BR local0 " through " local7 " (case-insensitive),"
or a decimal facility number.
.TP
.BI "tag " string ;
Tag syslog messages with \fIstring\fR.  Normally the messages are tagged with
the program name.
.TP
.BI "print\-priority " bool ;
Prefix each message with its priority.
.PP
An example \fBsyslog\fR statement:
.PP
.RS
.EX
syslog {
    facility local0;
    print-priority yes;
}
.EE
.RE
.SH NOTE
This manpage is a short description of \fBvcsync.conf\fR
configuration fike.  For a detailed discussion, including examples and
usage recommendations, refer to the \fBVcsync User Manual\fR available
in texinfo format.  If the \fBinfo\fR reader and the \fBvcsync\fR
documentation are properly installed on your system, the command
.PP
.RS +4
.B info vcsync.conf
.RE
.PP
should give you access to the complete manual.
.PP
You can also view the manual using the info mode in
.BR emacs (1),
or find it in various formats online at
.PP
.RS +4
.B http://www.gnu.org.ua/software/vcsync
.RE
.PP
If any discrepancies occur between this manpage and the
\fBVcsync User Manual\fR, the later shall be considered the authoritative
source.
.SH "SEE ALSO"
.BR vcsync (8).
.SH AUTHORS
Sergey Poznyakoff <gray@gnu.org>
.SH "BUG REPORTS"
Report bugs to <bug-vcsync@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2014--2024 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

