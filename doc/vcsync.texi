\input texinfo @c -*-texinfo-*-
@c %**start of header
@setfilename vcsync.info
@settitle Vcsync User Manual
@c %**end of header
@setchapternewpage odd

@defcodeindex pr
@defcodeindex op
@defcodeindex kw
@defcodeindex fl

@syncodeindex fn cp
@syncodeindex vr cp
@syncodeindex ky cp
@syncodeindex pg cp
@syncodeindex tp cp
@syncodeindex op cp
@syncodeindex pr cp
@syncodeindex kw cp
@syncodeindex fl cp

@include version.texi

@ifinfo
@dircategory System Administration
@direntry
* vcsync: (vcsync).           Synchronize files on check-ins to a repository.
* vcsync.conf: (vcsync) Configuration.  Configuration of the vcsync tool.
@end direntry
@end ifinfo

@copying
Copyright @copyright{} 2014--2024 Sergey Poznyakoff

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, and with Front-Cover and Back-Cover at your option.
A copy of the license is included in the section entitled ``GNU Free
Documentation License''.
@end copying

@titlepage
@title Vcsync User Manual
@subtitle version @value{VERSION}, @value{UPDATED}
@author Sergey Poznyakoff.
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@contents

@ifnottex
@node Top
@top Vcsync

This edition of the @cite{Vcsync User Manual}, last updated @value{UPDATED},
documents Vcsync Version @value{VERSION}.
@end ifnottex

@node Overview
@chapter Overview
It is often necessary to maintain a directory tree which is an exact
copy of the data stored in a version control system repository.

The synopsis is as follows.  There is a version control system
repository, whose pathname is @file{@var{repo_root}/@var{project}} and a
directory @file{@var{dest_root}/@var{project}}, called
@dfn{destination directory}.  The task is to ensure that each check in
to the repository be immediately reflected in the destination directory.

@command{vcsync} is designed to solve that task for CVS, SVN and Git.
To do so, it is installed as a hook that is invoked upon each
modification of the repository.  It takes the information about the
@acronym{VCS} in use, repository and destination paths either from the
command line options or from its configuration file. 

@node CVS
@section CVS
@cindex CVS
@flindex CVSROOT/loginfo
When used with CVS, the program is invoked from the
@file{CVSROOT/loginfo} file as follows@footnote{If using old CVS
format strings, the following line should be used instead:

@example
ALL /usr/bin/vcsync --vcs-type=cvs --detach --sleep=2 -- %s
@end example

@noindent
Such usage, however, is not recommended.
}:

@example
ALL /usr/bin/vcsync --vcs-type=cvs --detach --sleep=2 -- %p %s
@end example

The @option{--vcs-type} option tells @command{vcsync} which
@acronym{VCS} to use.  You can omit it, as @command{vcsync}
is able to determine the kind of @acronym{VCS} it has been called
from (@pxref{VCS detection}).

The @option{--detach} and @option{--sleep} options instruct it to
detach from the controlling terminal right after start up and wait for
two seconds before performing its job.  These two options avoid
dead-locking condition which occurs when @command{cvs update} is
invoked from the @file{loginfo} file.

The two dashes mark end of options.  This is a precaution for cases
when expansion of @code{%p} begins with a dash.

These options can be stored in the configuration file
(@pxref{Configuration}), which will look like:

@example
# Define the VCS type (optional).
vcs-type cvs;
# Detach from the controlling terminal.
detach yes;
# Wait for the check in process to terminate.
sleep 2;
# Destination directory is located in this directory.
destination-root /srv/home;
@end example

@noindent
Given this configuration file, the line in @file{CVSROOT/loginfo} can
be simplified to:

@example
ALL /bin/vcsync -- %p %s
@end example

The CVS synchronization routine gets the name of the project (the
@var{project} part discussed in @pxref{Overview}) from the value of the 
environment variable @env{CVSROOT}, by selecting its trailing
directory component.  For example, given the configuration file above
and the setting @env{CVSROOT=/var/cvsroot/foo}, @command{vcsync} will
assume the destination directory to be @file{/srv/home/foo}.

@node SVN
@section SVN
@cindex SVN
@cindex post-commit, SVN
When using SVN, @command{vcsync} must be configured as a @file{post-commit}
hook.  It expects the same arguments as the hook, i.e. path to the
repository as the first argument, and revision string as the second
one.  Given the proper configuration file, it can be set up as:

@example
$ cd repo/hooks
$ ln -sf /usr/bin/vcsync post-commit
@end example

@noindent
The configuration file should then contain at least:

@example
destination-root /srv/home;
@end example

The project name is deduced from the first argument, by selecting its
trailing directory component.

@node Git
@section Git
@cindex git
@cindex post-receive, git
When used with Git, @command{vcsync} must be invoked as @file{post-receive}
hook.  For example:

@example
$ cd repo.git/hooks
$ ln -s /usr/bin/vcsync post-receive
@end example

@noindent
The simplest configuration file is:

@example
destination-root /srv/home;
@end example

The project name is obtained from the last directory component of the
current working directory (which is set by @command{git} to the repository
being modified).  The @code{.git} suffix will be removed.

@node Configuration
@chapter Vcsync Configuration

@findex vcsync.conf
The program reads its configuration from the file @file{/etc/vcsync.conf}
(the exact location can be changed at compilation time).  Since most of
the settings can be given in the command line, this file is optional
and it is not considered an error if it does not exist (the warning
about the fact is displayed if @command{vcsync} is run with the
debugging level 1 or higher, though).  An alternative configuration
file can be specified using the @option{-c} (@option{--config})
command line option.

The file consists of statements terminated with semicolons.
White-space characters (space, horizontal and vertical tabulation,
newline, form-feed) are ignored, except as they serve to separate the
tokens.  Empty lines and comments are ignored.  There are two kinds of
comments: single-line and multi-line.  Single-line comments start
with @samp{#} or @samp{//} and continue to the end of the line.
Multi-line or C-style comments start with the two characters @samp{/*}
and continue until the first occurrence of @samp{*/}.

For a detailed discussion of the configuration file syntax,
@ref{Configuration file syntax}.  In the sections that follow, we will
describe the statements understood by @command{vcsync}.  Whenever a
command line option exists that overrides a given configuration
statement, that option will be listed with the statement in question.

@node Synchronization settings
@section Synchronization settings

@deffn {Config} vcs-type @var{type}
Sets the type of @acronym{VCS} to use.  Valid types are: @samp{cvs}, @samp{svn},
and @samp{git}.  This statement is usually not needed, as
@command{vcsync} is able to determine the @acronym{VCS} type
automatically.  @xref{VCS detection}.

The corresponding command line option is @option{-T} (@option{--type}).
@end deffn

@deffn {config} vcs-command @var{command}
Sets the @acronym{VCS} command line.  The default @var{command}
depends on the @acronym{VCS} type:

@multitable @columnfractions 0.30 .5
@item CVS  @tab /usr/bin/cvs -q -z3
@item SVN  @tab /usr/bin/svn
@item Git  @tab /usr/bin/git
@end multitable
@end deffn

@deffn {Config} destination-root @var{dir}
Sets the pathname of the of the destination root directory.

This can be set from the command line using the @option{-w}
(@option{--destination-root}) option.
@end deffn

@deffn {Config} timeout @var{number}
Command execution timeout (seconds).  If the @acronym{VCS} command
takes more than this number of seconds to run, it will be forcibly
terminated.
@end deffn

@deffn {Config} detach @var{boolean}
Detach from the controlling terminal before synchronizing.  @xref{CVS}
for a discussion of this setting.

The corresponding command line option is @option{-b} (@option{--detach}).
@end deffn

@deffn {Config} sleep @var{number}
Seconds to sleep before synchronizing.  @xref{CVS} for a discussion of
this setting.

The corresponding command line option is @option{-s} (@option{--sleep}).
@end deffn

@deffn {Config} message @var{string}
Defines the message to display before synchronizing.  The message will
be printed on standard error.
@end deffn

@node Foreign destinations
@section Foreign destinations
@cindex foreign destination
@cindex destination, foreign
@cindex destination not under VCS
A destination directory is called @dfn{foreign} if it was not created
as a result of checkout from the VCS that triggered its update.
Such foreign destinations can occur, for example, if CVS was initially
used as the version control system, but then it got replaced by git.

The default behavior of @command{vcsync} upon encountering a foreign
destination directory is to abort synchronization.  The
@code{foreign-dir-mode} configuration statement changes this default.

@deffn {Config} foreign-dir-mode mode
Defines the action to be taken upon encountering a foreign
destination.  The @var{mode} argument can be one of the following:

@table @code
@item error
Issue error diagnostic message and abort the syncronization.  This is
the default behavior.

@item remove
Remove the foreign directory and continue with a clean checkout from
the VCS.

@item rename
Rename the foregn directory and continue with a clean checkout from
the VCS.  New name is constructed as

@example
@var{dirname}-@var{uniq}.vcsync
@end example

@noindent
where @var{dirname} is the original directory name, and @var{uniq} is a
6-character sequence of alphanumeric characters selected so as to make
the name unique within the directory.

@item archive
Create an archive of the foreign directory, remove the directory, and
proceed with the checkout.  The archive is created using an external
command defined by the @code{archive-command} configuration statement.
@end table
@end deffn

@deffn {Config} archive-command command
External command used if @code{foreign-dir-mode} is set to
@samp{archive}. The @env{$DIRNAME} variable will be expanded to the
name of the destination directory.  No other expansions take place.

For example:

@example
archive-command "tar rf /var/backups/$DIRNAME.tar $DIRNAME";
@end example
@end deffn

@node Disabling synchronization
@section Disabling synchronization
@cindex synchronization, disabling
@cindex sentinel

@command{vcsync} can be instructed to disable synchronization for a
particular destination directory.  To do so, define the name of
a special @dfn{sentinel file}.  Before proceeding, @command{vcsync}
will check if that file exists in the destination directory.  If it
does exist, synchronization will be skipped and @command{vcsync} will
exit with code 0, indicating successful operation.

@deffn {Config} sentinel @var{filename}
Defines the name of the sentinel file.  The argument must be a valid
file name without directory part.
@end deffn

Before exiting, the following message will be output on standard
error:

@example
@var{destdir} is not under VCS control: sentinel file exists
@end example

@noindent
where @var{destdir} is the destination directory name.  This message
can be seen when using Git.

@node Notification
@section Notification
@cindex notification
@cindex tcpmux
@command{vcsync} can be configured to send a @dfn{notification} over
@code{INET} or @code{UNIX} sockets, after completing user
request. 

The notification protocol is based on @acronym{TCPMUX}
(@uref{http://www.rfc-editor.org/rfc/rfc1078.txt, RFC 1078}).

After establishing connection to the @dfn{notification socket}
declared in the configuration, @command{vcsync} sends the predefined
@dfn{tag} followed by a CRLF pair.  The remote party replies with a
single character indicating positive (@samp{+}) or negative (@samp{-})
acknowledgment, optionally followed by a message of explanation, and
terminated with a CRLF.

If positive acknowledgment is received, @command{vcsync} sends in
reply the destination directory name followed by CRLF.  After this
line, it sends names of all subdirectories changed by this
synchronization.  Each directory name is sent on a separate line
terminated with CRLF.  After this, @command{vcsync} closes connection.

Notification is configured using the following two @code{post-sync}
statements:

@deffn {Config} post-sync-socket @var{url}
Send notification to @var{url} about successful synchronization.

Allowed formats for @var{url} are:

@table @asis
@item inet://@var{hostname}[:@var{port}]
Connect to remote host @var{hostname} using TCP/IP.  @var{Hostname} is the
host name or IP address of the remote machine.  Optional @var{port}
specifies the port number to connect to.  It can be either a decimal
port number or a service name from @file{/etc/services}.  If
@var{port} is absent, @samp{tcpmux} (port 1) is assumed.

@item unix://@var{filename}
@itemx local://@var{filename}
Connect to a @code{UNIX} socket @var{filename}.
@end table
@end deffn

@deffn {Config} post-sync-tag @var{tag}
Define the @acronym{TCPMUX} service name to use.  The default is @samp{vcsync}.
@end deffn

@node Checkoutrc
@section The @file{.checkoutrc} file
A special feature is provided to use @command{vcsync} as a deployment
engine for web pages.  After synchronizing destination directory with
the repository, it scans each updated directory for the presence of
a special @dfn{access file} and file named @file{.checkoutrc}.

@cindex access file
@findex .htaccess
Access file is a directory-level configuration file for the
@command{httpd} server.  By default it is named @file{.htaccess}.
During postprocessing in each directory, @command{vcsync} removes the
existing access file and recreates it as directed by the statements in
the @file{.checkoutrc} file.

@deffn{Config} access-file-name @var{name}
Declares the name of access file to use instead of the default
@file{.htaccess}.
@end deffn

@deffn{Config} access-file-text @var{text}
Adds text to the beginning of each created access file.  It is common
to use the @dfn{here-document syntax} (@pxref{here-document}) for
@var{text}, e.g.:

@example
@group
access-file-text <<EOF
Options All -Indexes
Require all granted
EOF;
@end group
@end example
@end deffn

@findex .checkoutrc
If the @file{.checkoutrc} file is present in the directory, it is read
and processed line by line.  In each line, a @samp{#} character
introduces a comment.  The character and anything after it up to the nearest
newline is removed.  Empty lines are ignored.  Non-empty lines declare
modifications to the destination directory, that should be applied
after successful synchronization:

@deffn {checkoutrc} link @var{src} @var{dst}
Create a symbolic link from @var{src} to @var{dst}.  Neither argument
can be an absolute pathname or contain references to the parent
directory.
@end deffn

@deffn {checkoutrc} unlink @var{file}
Unlink the file.  The argument must refer to a file in the current
directory or one of its subdirectories, that is either a symbolic link
to another file or the access file.
@end deffn

@deffn {checkoutrc} chmod @var{mode} @var{file} [@var{file}...]
Change mode of the listed files to @var{mode}, which must be an octal
number.  Shell wildcards are allowed in @var{file} arguments.  After
expanding, each file argument must refer to a file in the current
directory or one of its subdirectories.
@end deffn

The administrator can also define additional keywords that, if
encountered in the @file{.checkoutrc} file, will cause addition of
certain text to the @file{.htaccess} file.  Such keywords are
case-insensitive.  They are defined using the following configuration file
statements.

@deffn {Config} define @var{keyword} @var{text}
Define keyword and its replacement text.  @var{text} is the material
which will be appended to @file{.htaccess} when this @var{keyword} is
encountered.

It is common to use the @dfn{here-document syntax}
(@pxref{here-document}) to define multi-line texts, e.g.:

@example
@group
define xbithack <<EOT
Options +IncludesNOEXEC
XBitHack On
EOT;
@end group
@end example
@end deffn

@deffn {Config} define @var{keyword}
Defines a keyword that will be reproduced in the @file{.htaccess} file
verbatim.  This is a shortcut for

@example
define @var{keyword} @var{keyword};
@end example
@end deffn

The following example illustrates the use of the @code{define}
configuration statement.  It is taken from the default
@file{vcsync.conf} file:

@example
define xbithack <<EOT
Options +IncludesNOEXEC
XBitHack On
EOT;

define ssi <<EOT
AddHandler server-parsed .html
EOT;

define redirect;
define rewritebase;
define rewritecond;
define rewriteengine;
define rewritemap;
define rewriteoptions;
define rewriterule;
@end example

@node Named configurations
@section Named configurations
A @dfn{named configuration} is a compound statement in the @file{vcsync.conf}
file that defines configuration settings to be used on request.  It is
defined as follows:

@kwindex config
@example
@group
config @var{name} @{
   @var{statements}
@}
@end group
@end example

@noindent
Here, @var{name} is a unique name identifying this configuration and
@var{statements} is one or more configuration statements discussed
above.

The use of a particular named configuration can be requested from the
command line using the @option{-N} (@option{--name}) command line
option, e.g.:

@example
vcsync --name=svn
@end example

Named configuration are also used if the @acronym{VCS} type is not
specified explicitly.  This is covered by the next section.

@node VCS detection
@section VCS detection
If the @acronym{VCS} type is not supplied explicitly (by the @code{vcs-type}
configuration setting or the @option{--vcs-type} option), the program
attempts to determine it by looking at the name by which it is
invoked.  It is able to correctly determine the @acronym{VCS} type, if
it is being run from the hooks subdirectory of a git or SVN
repository.  Otherwise, it will assume the default @acronym{VCS} type.

Once the @acronym{VCS} type is established, @command{vcsync} will scan
the configuration file for named configurations (@pxref{Named
configurations}).  In each named configuration, it will look for the
@code{vcs-type} statement.  If that statement is present and its value
matches the determined @acronym{VCS} type, the settings from this
configuration block will be used.

If no matching named configuration is found, the default one will be used.

Thus, a single configuration file can be used for all three supported
version control systems.

@node Logging and debugging
@section Logging and debugging
By default @command{vcsync} logs errors to the syslog facility
@samp{user}.  This can be modified both in the configuration and
from the command line.  In configuration file, logging is configured
using the following @dfn{compound statement}:

@kwindex syslog
@example
@group
syslog @{
   facility @var{string};
   tag @var{string};
   print-priority @var{bool};
@}
@end group
@end example

The statements within the @code{syslog} block are:

@deffn {syslog} facility @var{string}
Sets the syslog facility to use.  Valid arguments are: @code{user},
@code{daemon}, @code{auth} or @code{authpriv}, @code{mail},
@code{cron}, @code{local0} through @code{local7} (case-insensitive),
or a decimal facility number.
@end deffn

@deffn {syslog} tag @var{string}
Tags syslog messages with this string.  Normally the messages are
tagged with the program name.
@end deffn

@deffn {syslog} print-priority @var{bool}
If @var{bool} is @samp{true}, prefix each message with its priority.
True values are: @code{true}, @code{yes}, @code{t} or @code{1}.  False
values are: @code{false}, @code{no}, @code{nil}, @code{0}.
@end deffn

The @option{-e} (@option{--stderr}) option disables syslog output and
redirects all diagnostics to standard error instead.

The @option{-l @var{pri}} command line option instructs
@command{vcsync} to duplicate on the standard error all syslog
messages with priority @var{pri} or higher.

The verbosity of @command{vcsync} is controlled by the @code{debug}
statement:

@deffn {Config} debug @var{number}
Sets debugging verbosity level.  The bigger @var{number} is, the more
information will be logged.
@end deffn

The same effect is achieved by the @option{-d} (@option{--debug})
option.  This option is incremental, i.e. single option sets minimal
verbosity, two @option{-d} set level 2, and so on.

@node Invocation
@chapter Invocation

The following table summarizes command line options understood by
@command{vcsync}.

@table @option
@opindex -b
@opindex --detach
@item -b
@itemx --detach
Detach from the controlling terminal after startup.  This is mostly for
use with CVS (together with the @option{-s} option).  @xref{CVS}.

@opindex -D
@opindex --checkoutrc
@item -D @var{dir}
@itemx --checkoutrc=@var{dir}
Look for the @file{.checkoutrc} file in directory @var{dir}.  If
found, process it.  If @var{dir} ends with a slash, descend
recursively into its subdirectories and process any @file{.checkoutrc}
files found.  @xref{Checkoutrc}, for a detailed discussion of
@file{.checkoutrc} files and their purpose.

@opindex -d
@opindex --debug
@item -d
@itemx --debug
Increase debugging verbosity.  @xref{Logging and debugging}.

@opindex -x
@opindex --config
@item -c @var{file}
@itemx --config=@var{file}
Read configuration from @var{file} instead of the default location.

@opindex -e
@opindex --stderr
@item -e
@itemx --stderr
Log everything to standard error instead of the syslog.

@opindex -H
@opindex --config-help
@item -H
@itemx --config-help
Display help on the configuration file syntax and exit.

@opindex -h
@opindex --help
@item -h
@itemx --help
Print a short help summary and exit.

@opindex -l
@item -l @var{prio}
Log everything with priority @var{prio} and higher to the stderr, as
well as to the syslog.

@opindex -N
@opindex --name
@item -N @var{name}
@itemx --name=@var{name}
The configuration file can contain distinct sets of settings, called
@dfn{named configurations}.  This option instructs @command{vcsync}
to read configuration from the set @var{name}.  @pxref{Named
configurations}, for a detailed description of this feature.

@opindex -n
@opindex --dry-run
@item -n
@itemx --dry-run
Do nothing, only print what would have been done.

@opindex -T
@opindex --vcs-type
@item -T @code{cvs} | @code{svn} | @code{git}
@itemx --vcs-type=@code{cvs} | @code{svn} | @code{git}
Sets the @acronym{VCS} type.  Normally, this option is not needed, as
@file{vcsync} is able to determine the @acronym{VCS} type
automatically.  @xref{VCS detection}.

@opindex -t
@opindex --lint
@item -t
@itemx --lint
Check the configuration file syntax and exit.

@opindex -s
@opindex --sleep
@item -s @var{n}
@itemx --sleep=@var{n}
Sleep @var{n} seconds before doing the job.  @xref{CVS}.

@opindex -w
@opindex --destination-root
@item -w @var{dir}
@itemx --destination-root=@var{dir}
Defines destination root directory.  @xref{Synchronization settings}.

@opindex -V
@opindex --version
@item -V
@itemx --version
Print program version and ext.

@opindex --usage
@item --usage
Print a terse summary of the command line syntax and exit.
@end table

@node Reporting Bugs
@chapter Reporting Bugs

Please, report bugs and suggestions to @email{bug-vcsync@@gnu.org.ua}.

  You hit a bug if at least one of the conditions below is met:

@itemize @bullet
@item @command{vcsync} terminates on signal 11
(SIGSEGV) or 6 (SIGABRT).
@item The program fails to do its job as described in
this manual.
@end itemize

  If you think you've found a bug, please be sure to include maximum
information available to reliably reproduce it, or at least to analyze
it.  The information needed is:

@itemize
@item Version of the package you are using.
@item Command line options and configuration file.
@item Conditions under which the bug appears.
@end itemize

  Any errors, typos or omissions found in this manual also qualify as
bugs.  Please report them, if you happen to find any.

@node Configuration file syntax
@appendix Configuration file syntax
@raisesections
@set PROGNAME vcsync
@include grecs-syntax.texi
@lowersections

@node Downloads
@appendix Downloading vcsync
Stable releases of @command{vcsync} can be downloaded from
@uref{http://download.gnu.org.ua/pub/release/vcsync}
@footnote{Traditionally, the @code{FTP} protocol is supported as well:
@*
@uref{ftp://download.gnu.org.ua/pub/release/vcsync}}.

Each tarball in this directory is GPG signed and the corresponding
signature is available in file with the @file{.sig} suffix.  To verify
the tarball, be sure to download both the tarball and its signature
(e.g., for the latest version:
@ifhtml
@uref{http://download.gnu.org.ua/pub/release/vcsync/vcsync-@value{VERSION}.tar.gz,
vcsync-@value{VERSION}.tar.gz}
and
@uref{http://download.gnu.org.ua/pub/release/vcsync/vcsync-@value{VERSION}.tar.gz.sig,
vcsync-@value{VERSION}.tar.gz.sig}
@end ifhtml
@ifnothtml
@file{vcsync-@value{VERSION}.tar.gz} and
@file{vcsync-@value{VERSION}.tar.gz.sig}
@end ifnothtml
).  Then run a command like this:

@example
gpg --verify vcsync-@value{VERSION}.tar.gz.sig
@end example

If that command fails because you don't have the author's public
key, then run this command to import it: 

@example
gpg --keyserver keys.gnupg.net --recv-keys 3602B07F55D0C732
@end example

A symbolic link to the latest release (and its signature) is provided: 
@uref{http://download.gnu.org.ua/pub/release/vcsync/vcsync-latest.tar.gz}.

Pre-release and alpha versions of the package can be downloaded from
@uref{http://download.gnu.org.ua/pub/alpha/vcsync}.

Finally, development of the @command{vcsync} can be tracked from
@uref{https://puszcza.gnu.org.ua/projects/vcsync}.

@node Copying This Manual
@appendix GNU Free Documentation License
@include fdl.texi

@node Index
@unnumbered Index

This is a general index of all issues discussed in this manual

@printindex cp

@ifset WEBDOC
@ifhtml
@node This Manual in Other Formats
@unnumbered This Manual in Other Formats
@include otherdoc.texi
@end ifhtml
@end ifset

@bye

