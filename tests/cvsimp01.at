# This file is part of Vcsync testsuite. -*- Autotest -*-
# Copyright (C) 2014-2024 Sergey Poznyakoff
#
# Vcsync is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Vcsync is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([initial import with checkoutrc])
AT_KEYWORDS([cvs cvsimp cvsimp01 checkoutrc checkoutrc04])

AT_CHECK([
AT_VCS_PREREQ(CVS)
cwd=$(pwd)
CVS_COM_INIT([
cat > .checkoutrc <<EOT
link README README.html
EOT
cat > dir/.checkoutrc <<EOT
chmod 755 file
xbithack on
EOT
])
echo "Home dir"
find home/test | sed '/CVS\//d' | sort
echo "Created files"
find home/test -type l
find home/test \( -not -path '*/CVS/hooks/*' \) -type f -perm 755
echo "File dir/.htaccess"
cat home/test/dir/.htaccess
],
[0],
[Home dir
home/test
home/test/.checkoutrc
home/test/CVS
home/test/README
home/test/README.html
home/test/dir
home/test/dir/.checkoutrc
home/test/dir/.htaccess
home/test/dir/CVS
home/test/dir/file
Created files
home/test/README.html
home/test/dir/file
File dir/.htaccess
Options +IncludesNOEXEC
XBitHack On
],
[stderr])

# Remove debugging statements from stderr. They will be displayed if the
# user sets VCSYNC_TESTSUITE_DEBUG when testing.
AT_CHECK([sed '/vcsync: \[[DEBUG\]]/d' stderr])

AT_CLEANUP

