# This file is part of Vcsync testsuite. -*- Autotest -*-
# Copyright (C) 2024 Sergey Poznyakoff
#
# Vcsync is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Vcsync is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.

AT_BANNER([CVS foreign directory handling])

AT_SETUP([error])
AT_KEYWORDS([cvs cvsfor cvsforerror foreign])

AT_CHECK([
AT_VCS_PREREQ(CVS)
cwd=$(pwd)
CVS_COM_INIT

# Pretend it's not CVS-controlled
set -e
rm -r home/test/CVS

cd test
echo "new file" > file
$CVS_BIN -Q add file
$CVS_BIN -Q commit -m 'Add new file' > /dev/null
test -f home/test/file
],
[1],
[],
[stderr])

# Remove debugging statements from stderr. They will be displayed if the
# user sets VCSYNC_TESTSUITE_DEBUG when testing.
AT_CHECK([sed '/vcsync: \[[DEBUG\]]/d' stderr])
AT_CLEANUP

# VCSYNC_CHECK_FOREIGN(mode,addcfg,addtest)
#                       $1    $2      $3
m4_pushdef([VCSYNC_CHECK_FOREIGN],
[AT_SETUP([$1])
AT_KEYWORDS([cvs cvsfor cvsfor$1 foreign])
AT_CHECK([
AT_VCS_PREREQ(CVS)
cwd=$(pwd)
CVS_COM_INIT([],[foreign-dir-mode "$1";
$2
])

# Pretend it's not CVS-controlled
set -e
rm -r home/test/CVS

(cd test
echo "new file" > file
$CVS_BIN -Q add file
$CVS_BIN -Q commit -m 'Add new file' > /dev/null
sleep 3)

cat home/test/file
],
[0],
[new file
],
[stderr])

# Remove debugging statements from stderr. They will be displayed if the
# user sets VCSYNC_TESTSUITE_DEBUG when testing.
AT_CHECK([sed '/vcsync: \[[DEBUG\]]/d' stderr])
$3
AT_CLEANUP
])

VCSYNC_CHECK_FOREIGN([remove])

VCSYNC_CHECK_FOREIGN([rename],[],
[AT_CHECK([find home/test-??????.vcsync | \
  sed -e '/^.*\/test-......\.vcsync$/d' -e 's|^.*/test-......\.vcsync/||' | sort
],
[0],
[README
dir
dir/CVS
dir/CVS/Entries
dir/CVS/Repository
dir/CVS/Root
dir/file
])])

VCSYNC_CHECK_FOREIGN([archive],
 [archive-command "tar cf $cwd/\$DIRNAME.tar \$DIRNAME";],
 [AT_CHECK([tar tf test.tar | sort],
[0],
[test/
test/README
test/dir/
test/dir/CVS/
test/dir/CVS/Entries
test/dir/CVS/Repository
test/dir/CVS/Root
test/dir/file
])])

m4_popdef([VCSYNC_CHECK_FOREIGN])


