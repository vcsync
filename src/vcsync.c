/* This file is part of Vcsync
   Copyright (C) 2014-2024 Sergey Poznyakoff
  
   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "vcsync.h"
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

int dry_run;
int debug_level;
int lint_only;
int stderr_only;
char *config_file;
char *destination_root;
unsigned sleep_option;
int detach_option;
char const *vcs_type;

static struct vcs_defn vcs_tab[] = {
	{ "cvs", cvs_proc, cvs_dir_ok },
	{ "svn", svn_proc, svn_dir_ok },
	{ "git", git_proc, git_dir_ok },
	{ NULL }
};

struct vcs_defn const *
vcs_defn_get(const char *name)
{
	struct vcs_defn *p;

	for (p = vcs_tab; p->name; p++)
		if (strcmp(p->name, name) == 0)
			return p;
	
	return NULL;
}

char *
pathcat(const char **path)
{
	size_t len = 0;
	const char **pp;
	char *ret, *p;
	const char *q;
	
	for (pp = path; *pp; pp++)
		len += strlen(*pp) + !!pp[1];

	ret = emalloc(len + 1);
	for (pp = path, p = ret; *pp; pp++) {
		for (q = *pp; (*p = *q); p++, q++)
			;
		if (pp[1])
			*p++ = '/';
	}
	*p = 0;
	
	return ret;
}
	
char *
mkfilename(const char *dir, const char *name)
{
	const char *a[3];
	
	a[0] = dir;
	a[1] = name;
	a[2] = NULL;
	return pathcat(a);
}

char *
parsefilename(const char *fname, char **basename)
{
	char *retval;
	size_t len;

	len = strlen (fname);
	while (len > 0 && fname[len-1] == '/')
		--len;

	if (len == 0) {
		retval = emalloc(2);
		retval[0] = '/';
		retval[1] = 0;

		if (basename)
			*basename = NULL;
	} else {
		size_t i;

		for (i = len - 1; i > 0 && fname[i-1] != '/'; i--)
			;

		if (i) {
			if (basename) {
				size_t baselen = len - i;
				*basename = emalloc(baselen + 1);
				memcpy(*basename, fname + i, baselen);
				(*basename)[baselen] = 0;
			}

			while (i > 0 && fname[i-1] == '/')
				i--;

			retval = emalloc(i + 1);
			memcpy(retval, fname, i);
			retval[i] = 0;
		} else {
			if (basename)
				*basename = estrdup(fname);
			retval = estrdup(".");
		}
	}

	if (strcmp(retval, ".") == 0) {
		free(retval);
		retval = get_cwd();
	} else if (strncmp(retval, "./", 2) == 0 || retval[0] != '/') {
		char *p = mkfilename(get_cwd(),
				     retval + (retval[0] == '.' ? 2 : 0));
		free(retval);
		retval = p;
	}
	return retval;
}

int
string_suffix(char const *str, char const *suf)
{
	char const *estr;
	char const *esuf;

	if (!str || !suf)
		return 0;

	estr = str + strlen (str) - 1;
	esuf = suf + strlen (suf) - 1;
	while (estr >= str && *estr == *esuf) {
		if (esuf == suf)
			return 1;
		esuf--;
		estr--;
	}
	return 0;
}

char *
get_cwd(void)
{
	char *ret;
	unsigned path_max;
	char buf[128];

	errno = 0;
	ret = getcwd(buf, sizeof(buf));
	if (ret != NULL)
		return estrdup(buf);

	if (errno != ERANGE)
		die(EX_FAIL, "can't get cwd: %s", strerror(errno));

	path_max = 128;
	path_max += 2;                /* The getcwd docs say to do this. */

	for (;;) {
		char *cwd = emalloc(path_max);
		errno = 0;
		ret = getcwd(cwd, path_max);
		if (ret)
			break;
		if (errno != ERANGE)
			die(EX_FAIL, "can't get cwd: %s", strerror(errno));
		free(cwd);

		path_max += path_max / 16;
		path_max += 32;
	}
	return ret;
}

/*
if dirname == 'hooks'
  # Naive approach
  if basename == 'post-commit'
    type = svn
  elif basename == 'post-receive'
    type = git
  else
    scan uplevel dirname
    if has_dirs('branches', 'objects', 'refs', 'info')
       type = git
    elif has_dirs('conf', 'dav', 'db') and has_file('format')
       type = svn
    endif
  endif
endif

if type == undef
  type = default_type
endif
*/

#define TYPE_GIT "git"
#define TYPE_SVN "svn"
#define TYPE_CVS "cvs"

struct file_type {
	char const *name;
	int type;
};
struct vcs_files {
	char const *vcs;
	struct file_type *file_types;
	int file_count;
	int found_count;
};
static struct file_type git_files[] = {
	{ "branches", S_IFDIR },
	{ "objects",  S_IFDIR },
	{ "refs",     S_IFDIR },
	{ "info",     S_IFDIR }
};
static struct file_type svn_files[] = {
	{ "conf",     S_IFDIR },
	{ "dav",      S_IFDIR },
	{ "db",       S_IFDIR },
	{ "format",   S_IFREG }
};
static struct vcs_files vcs_files[] = {
	{ "git", git_files, sizeof(git_files)/sizeof(git_files[0]) },
	{ "svn", svn_files, sizeof(svn_files)/sizeof(svn_files[0]) },
	{ NULL }
};

static char const *
identify_file(char const *dir, char const *file)
{
	struct vcs_files *vf;

	for (vf = vcs_files; vf->vcs; vf++) {
		int i;

		for (i = 0; i < vf->file_count; i++) {
			if (vf->found_count == -1)
				continue;
			if (strcmp (file, vf->file_types[i].name) == 0) {
				char *fn = mkfilename(dir, file);
				struct stat st;
				if (stat(fn, &st))
					continue;
				if (st.st_mode & vf->file_types[i].type) {
					vf->found_count++;
					if (vf->found_count == vf->file_count)
						return vf->vcs;
				} else
					vf->found_count = -1;
			}
		}
	}
	return NULL;
}

char const *
guess_vcs_type(void)
{
	char *dirname, *basename;
	char const *type = NULL;

	dirname = parsefilename(fullprogname, &basename);
	if (string_suffix(dirname, "/hooks") && basename) {
		/* Either git or svn */
		if (strcmp(basename, "post-receive") == 0)
			type = TYPE_GIT;
		else if (strcmp(basename, "post-commit") == 0)
			type = TYPE_SVN;
		else {
			DIR *dir;
			char *updir = parsefilename(dirname, NULL);

			dir = opendir(updir);
			if (dir) {
				struct dirent *ent;

				while ((ent = readdir(dir))) {
					type = identify_file(updir,
							     ent->d_name);
					if (type)
						break;
				}
			}
			closedir(dir);
			free(updir);
		}
	}
	return type;
}

#include "cmdline.h"

int
main(int argc, char **argv)
{
	int i;
	struct grecs_node *tree = NULL;

	setprogname(argv[0]);
	config_init();

	parse_options(argc, argv, &i);
	argc -= i;
	argv += i;

	if (!config_file) {
		if (access(DEFAULT_CONFIG_FILE, F_OK) == 0)
			config_file = DEFAULT_CONFIG_FILE;
		else {
			if (lint_only)
				die(EX_USAGE,
				    "configuration file %s doesn't exist",
				    DEFAULT_CONFIG_FILE);
			debug(1, ("default configuration file %s does not "
				  "exist", DEFAULT_CONFIG_FILE));
		}
	}

	if (config_file) {
		tree = grecs_parse(config_file);
		if (!tree)
			exit(EX_CONFIG);
	}
	
	config_finish(tree);
	if (lint_only)
		return EX_OK;

	if (have_checkoutrc()) {
		diag(LOG_INFO, "testing checkoutrc files");
		flush_checkoutrc_files(NULL);
		exit(EX_OK);
	}

	/* Fixup options */
	if (config->detach)
		stderr_only = 0;

        /* Reset log_to_stderr to whatever the user wants (if -l was
	   given), or to the default.
        */
	log_to_stderr = cli_log_to_stderr;
	
	if (!stderr_only && cfg_facility > 0) {
		openlog(cfg_tag, LOG_PID, cfg_facility);
		grecs_log_to_stderr = 0;
	}

	if (!config->name)
		debug(1, ("using default configuration for VCS %s",
			  config->vcs_type));
	else
		debug(1, ("using configuration \"%s\" for VCS %s",
			  config->name, config->vcs_type));

	if (config->message && *config->message) {
		fputs(config->message, stderr);
		if (config->message[strlen(config->message)-1] != '\n')
			fputc('\n', stderr);
	}
	
	if (config->detach) {
		if (daemon(1, 1))
			die(EX_FAIL, "daemon: %s", strerror(errno));
		log_to_stderr = 0;
	}

	if (config->sleep_time)
		sleep(config->sleep_time);

	flush_checkoutrc_files(config->vcs->proc(argc, argv));
	return EX_OK;
}
