/* This file is part of Vcsync
   Copyright (C) 2014-2024 Sergey Poznyakoff

   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <config.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "wordsplit.h"
#include "grecs.h"
#include "syslog.h"

#ifndef DEFAULT_VCS
# define DEFAULT_VCS "cvs"
#endif

#define DEFAULT_CONFIG_FILE SYSCONFDIR "/vcsync.conf"

#define EX_OK     0 
#define EX_FAIL   1
#define EX_USAGE  2
#define EX_CONFIG 3

#define MAX_CHECKOUTRC_KW 512

extern int debug_level;
extern int dry_run;
extern const char *fullprogname;
extern const char *progname;
extern int log_to_stderr;
extern char *destination_root;
extern unsigned sleep_option;
extern int detach_option;
extern char const *vcs_type;

void setprogname(const char *arg);
void vdiag(int prio, const char *fmt, va_list ap);
void diag(int prio, const char *fmt, ...);
void debugprt(const char *fmt, ...);
void die(int ex, const char *fmt, ...);
void vcsync_print_grecs_diag(grecs_locus_t const *locus, int err, int errcode,
			      const char *msg);

#define debug(l,c) do { if (debug_level >= (l)) debugprt c; } while (0)

#define emalloc grecs_malloc
#define ecalloc grecs_calloc
#define erealloc grecs_realloc
#define estrdup grecs_strdup

struct vcsync_config;
typedef char *(*vcs_proc_fn)(int argc, char **argv);
typedef int (*vcs_ok_fn)(int);

struct vcs_defn {
	const char *name;
	vcs_proc_fn proc;
	vcs_ok_fn   is_dir_vcs;
};

struct vcsync_sockaddr {
	struct sockaddr *sa;
	socklen_t len;
};

struct vcsync_config {
	struct vcsync_config *next;
	char *name;
	int debug_level;
	char *vcs_type;
	struct vcs_defn const *vcs;
	char *vcs_command; /* FIXME: additional VCS parameters? */
	char *dest_root;
	unsigned timeout;
	unsigned sleep_time;
	int detach;
	char *message;
	struct vcsync_sockaddr post_sync_socket;
	char *post_sync_tag;
	char *sentinel_file;
	int foreign_dir_mode;
	char *archive_command;
};

extern struct vcsync_config default_config;
extern struct vcsync_config *config;
extern char const *config_name;

extern int cfg_facility;
extern int cfg_print_priority;
extern char *cfg_tag;
extern char *access_file_name;
extern char *access_file_text;

void config_help(void);
void config_init(void);
void config_finish(struct grecs_node *tree);

int get_facility(const char *arg, int *retval);
int get_priority(const char *arg, int *retval);

struct vcs_defn const *vcs_defn_get(const char *name);

char *cvs_proc(int argc, char **argv);
int cvs_dir_ok(int fd);

char *svn_proc(int argc, char **argv);
int svn_dir_ok(int fd);

char *git_proc(int argc, char **argv);
int git_dir_ok(int fd);

/* Operation modes */
typedef enum {
	VCSYNC_UPDATE,
	VCSYNC_CHECKOUT,
	VCSYNC_IGNORE
} VCSYNC_MODE;

/* Foreign directory handling modes. */
enum {
	FOREIGN_DIR_ERROR,
	FOREIGN_DIR_REMOVE,
	FOREIGN_DIR_RENAME,
	FOREIGN_DIR_ARCHIVE
};

struct transtab {
	char *name;
	int tok;
};

int trans_strtotok(struct transtab *tab, const char *str, int *ret);
char *trans_toktostr(struct transtab *tab, int tok);
char *trans_toknext(struct transtab *tab, int tok, int *next);
char *trans_tokfirst(struct transtab *tab, int tok, int *next);

typedef struct vcsync_cmd_stream *vcsync_cmd_stream_t;
int runcmd(char **argv, vcsync_cmd_stream_t *pstr);
int runcmd_free(vcsync_cmd_stream_t str);
ssize_t runcmd_getline(vcsync_cmd_stream_t str, char **pstr, size_t *psize);

char *pathcat(const char **path);
char *mkfilename(const char *dir, const char *name);
char *parsefilename(const char *fname, char **basename);
int string_suffix(char const *str, char const *suf);
char const *guess_vcs_type(void);
char *get_cwd(void);

void checkoutrc(const char *, const char *);
void register_directory(const char *dir, int recursive);
void flush_checkoutrc_files(const char *);
int have_checkoutrc(void);
int checkoutrc_register_keyword(const char *name, char *text);

void remove_hier(char const *name);
VCSYNC_MODE check_output_dir(char const *dirname);





