/* This file is part of Vcsync
   Copyright (C) 2024 Sergey Poznyakoff

   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "vcsync.h"
#include <sys/time.h>

static int
rename_tmp(char const *dirname, char *tmplname, size_t suflen)
{
	int rc;
	size_t len;
	char *carrybuf;
	char *p, *cp, *start, *end;
	static int first_call;
	static char randstate[256];
	static const unsigned char alphabet[] =
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	if (!first_call) {
		/* Initialize random number generator */
		struct timeval tv;
		gettimeofday(&tv, NULL);
		initstate(((unsigned long) tv.tv_usec << 16) ^ tv.tv_sec,
			  randstate, sizeof(randstate));
		first_call = 1;
	}
	setstate(randstate);
  
	/* Start with the last tmplname character before suffix */
	end = tmplname + strlen(tmplname) - suflen - 1;
	/* Fill X's with random characters */
	for (p = end; p >= tmplname && *p == 'X'; p--)
		*p = alphabet[random() % (sizeof(alphabet) - 1)];
	len = end - p;
	if (len == 0)
		return EINVAL;
	start = p + 1;

	carrybuf = malloc(len);
	if (!carrybuf)
		return ENOMEM;

	/* Fill in the carry buffer */
	memcpy(carrybuf, start, len);

	for (;;) {
		if ((rc = rename(dirname, tmplname)) == 0) {
			rc = 0;
			break;
		} else if (errno != EEXIST) {
			rc = errno;
			break;
		}

		for (p = start, cp = carrybuf;; p++, cp++) {
			char *q;
	  
			if (p == end)
				/* All permutation exhausted */
				return EEXIST;
			q = strchr((char*)alphabet, *p);
			if (!q)
				abort(); /* should not happen */
			*p = (q[1] == 0) ? alphabet[0] : q[1];
			if (*p != *cp)
				break;
		}
	}
	free(carrybuf);
	return rc;
}

#define SUFFIX ".vcsync"
#define SUFLEN (sizeof(SUFFIX)-1)
#define XCOUNT 6

void
safe_rename(char const *name)
{
	int rc;
	int len = strlen(name);
	char *tmplname = grecs_malloc(len + 1 + XCOUNT + SUFLEN + 1);
	memcpy(tmplname, name, len);
	tmplname[len] = '-';
	memset(tmplname + len + 1, 'X', XCOUNT);
	memcpy(tmplname + len + 1 + XCOUNT, SUFFIX, SUFLEN);
	tmplname[len+1+XCOUNT+SUFLEN] = 0;
	rc = rename_tmp(name, tmplname, SUFLEN);
	if (rc)
		die(EX_FAIL, "can't rename %s: %s", name, strerror(rc));
	diag(LOG_INFO, "renamed %s to %s", name, tmplname);
	free(tmplname);
}

void
mydbg(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
	va_end(ap);
}

void
archive(char const *dirname)
{
	struct wordsplit ws;
	const char *env[3] = {
		"DIRNAME",
		dirname,
		NULL
	};

	ws.ws_env = env;
	if (wordsplit(config->archive_command,
		      &ws,
		      WRDSF_NOCMD | WRDSF_QUOTE |
		      WRDSF_SQUEEZE_DELIMS | WRDSF_CESCAPES |
		      WRDSF_ENV | WRDSF_ENV_KV))
		die(EX_FAIL, "wordsplit: %s", wordsplit_strerror(&ws));

	if (runcmd(ws.ws_wordv, NULL))
		exit(EX_FAIL);
	
	wordsplit_free(&ws);
}

VCSYNC_MODE
check_output_dir(char const *dirname)
{
	int fd;
	int vcs_ok;
	
	fd = open(dirname, O_RDONLY | O_NONBLOCK | O_DIRECTORY);
	if (fd == -1) {
		if (errno == ENOENT) {
			diag(LOG_INFO,
			     "%s does not exist: attempting clean checkout",
			     dirname);
			return VCSYNC_CHECKOUT;
		} else
			die(EX_FAIL, "cannot access directory %s", dirname,
			    strerror(errno));
	}

	if (config->sentinel_file) {
		if (faccessat(fd, config->sentinel_file, F_OK, AT_SYMLINK_NOFOLLOW) == 0) {
			close(fd);
			return VCSYNC_IGNORE;
		} else if (errno != ENOENT)
			die(EX_FAIL, "cannot access sentinel file %s/%s",
			    dirname, config->sentinel_file, strerror(errno));
	}

	vcs_ok = config->vcs->is_dir_vcs(fd);
	close(fd);

	if (vcs_ok == -1)
		exit(EX_FAIL);
	
	if (!vcs_ok) {
		debug(1,("foreign_dir_mode=%d\n",config->foreign_dir_mode));
		/* Directory exists, but is not under control of this VCS. */
		switch (config->foreign_dir_mode) {
		case FOREIGN_DIR_ERROR:
			diag(LOG_INFO,
			    "directory %s exists, but is not under %s control",
			    dirname, config->vcs_type);
			die(EX_FAIL,
			    "directory %s exists, but is not under %s control",
			    dirname, config->vcs_type);
		
		case FOREIGN_DIR_REMOVE:
			remove_hier(dirname);
			break;
			
		case FOREIGN_DIR_RENAME:
			safe_rename(dirname);
			break;
			
		case FOREIGN_DIR_ARCHIVE:
			archive(dirname);
			remove_hier(dirname);
		}
		diag(LOG_INFO,
		     "%s cleaned up: attempting clean checkout",
		     dirname);
		return VCSYNC_CHECKOUT;
	}
	return VCSYNC_UPDATE;
}	

