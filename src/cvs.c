/* This file is part of Vcsync
   Copyright (C) 2014-2024 Sergey Poznyakoff
  
   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/

/* The idea behind this module comes from Loic Dachary's sv_sync_www.c.

   To synchronize web directories on each commit add the following to
   CVSROOT/loginfo:
  
       ALL  /bin/vcsync %{s}

   Make sure to add the following statements to your vcsync.conf file:

       # Detach from the controlling terminal
       detach yes;
       # Sleep two seconds before proceding.
       sleep 2;

   This is neccessary to avoid deadlocking, which occurs when cvs is
   trying to acquire the lock held by the commit process that invoked
   vcsync.

   The following description is taken from sv_sync_www.c:

   <quote>
      For test purpose:
      webcvs -n ' bla bla'
      webcvs -n 'foo/bar bla bla'
      webcvs -n 'f'\''oo bla bla'
      webcvs -n 'f'\''oo - New directory'
      webcvs -n 'bla - Imported sources'

      The idea is that it runs a cvs update -l (to prevent recursion) in the 
      directory where the commit was done. Since the command will be called
      once for each directory where a commit did some action there is no
      need for recursion. In the case of an import command this does not
      hold and a recursion must always be done since there is only one
      call to the script for a whole imported tree (this happens when the
      argument contains the Imported source string).

      The %{s} argument in loginfo invocation is a single argument that lists
      the directory and all the files involved. As a special case if the
      directory was added the file list is replaced by '- New directory'. This
      is lame since adding the files -, New and directory will produce the same
      effect, but it's unlikely. The same applies when a whole source tree is
      imported using cvs import in which case the file list is replaced by
      '- Imported sources'.

      There are three cases to take in account (topdir is the absolute path
      of the directory in which the CVS tree was extracted, subdirectory is
      the directory given in argument):
      - commit that modify the top level directory files
        cd topdir ; cvs update -l 
      - commit that adds a new directory or that import a whole source tree
        cd topdir ; cvs update 'subdirectory'
      - commit that modify files in a subdirectory
        cd topdir/subdirectory ; cvs update -l
   </quote>
   
*/

#include "vcsync.h"

static int
split_arg(char *arg, char **pdir)
{
	char *p;
	
	if ((p = strchr(arg, ' '))) {
		if (p == arg)
			*pdir = NULL;
		else {
			*pdir = arg;
			*p = 0;
		}
		++p;

		debug(3, ("cvs: dir=%s, file list=%s", arg, p));
		/* If this is a new directory or an import command,
		   then there are no files to collect in the argument list. */
		if (strcmp(p, "- New directory") == 0
		    || strcmp(p, "- Imported sources") == 0)
			return 0;
		
		return 1;
	}
	*pdir = NULL;
	return -1;
}

static void
readinput(void)
{
	char *inbuf = NULL;
	size_t insize = 0;
	while (grecs_getline(&inbuf, &insize, stdin) > 0) {
		size_t len = strlen(inbuf);

		if (len == 0)
			continue;
		if (inbuf[len-1] == '\n')
			inbuf[--len] = 0;
		
		debug(1, ("CVS: %s", inbuf));
	}
	free(inbuf);
}

char *
cvs_proc(int argc, char **argv)
{
	int files = 0;
	char *directory = NULL;
	struct wordsplit ws;
	char *cmd = NULL;
	size_t cmdsize;
	char *repo_root;

	if (!argc)
		die(EX_USAGE, "cvs: expected at least one argument");
	else if (argc == 1) {
		debug(1, ("cvs: (old style?) %s", argv[0]));
		files = split_arg(argv[0], &directory);
		if (files == -1)
			die(EX_FAIL, "cvs: cannot split argument %s", argv[0]);
	} else {
		directory = argv[0];
		files = argc > 1;
	}		
	debug(2, ("cvs: directory=%s, has files=%d",
		  directory ? directory : "", files));

	readinput();

	repo_root = getenv("CVSROOT");
	if (!repo_root)
		die(EX_FAIL, "CVSROOT not defined");
	debug(2, ("CVSROOT=%s",repo_root));
	/* Set up VCS-dependent defaults */
	if (!config->vcs_command)
		config->vcs_command = "/usr/bin/cvs -q -z3";

	if (chdir(config->dest_root))
		die(EX_FAIL, "cannot change to %s: %s",
		    config->dest_root, strerror(errno));

	if (directory) {
		if (strcmp(directory, "CVSROOT") == 0) {
			diag(LOG_NOTICE, "cvs: ignoring %s", directory);
			return NULL;
		}
	
		switch (check_output_dir(directory)) {
		case VCSYNC_IGNORE:
			fprintf(stderr,
				"%s is not under VCS control: sentinel file exists\n",
				directory);
			return NULL;

		case VCSYNC_CHECKOUT:
			files = 0;
			debug(1, ("checking %s/%s out from CVS",
				  config->dest_root,
				  directory));
			break;

		case VCSYNC_UPDATE:
			debug(1, ("updating %s/%s from CVS",
				  config->dest_root,
				  directory));
			break;

		default:
			abort();
		}
	} else if (!files)
		die(EX_USAGE, "bad CVS usage: %s", argv[0]);

	if (files) {
		if (chdir(directory))
			die(EX_FAIL, "cannot change to %s: %s",
			    directory, strerror(errno));
		grecs_asprintf(&cmd, &cmdsize, "%s update -l",
			       config->vcs_command);
	} else
		grecs_asprintf(&cmd, &cmdsize,
			       "%s -d %s checkout %s",
			       config->vcs_command,
			       repo_root, directory);

	if (wordsplit(cmd, &ws, WRDSF_DEFFLAGS))
		die(EX_FAIL, "wordsplit: %s", wordsplit_strerror(&ws));
	free(cmd);
	
	runcmd(ws.ws_wordv, NULL);

	wordsplit_free(&ws);

	if (directory)
		register_directory(directory, !files);
	return NULL;
}

int
cvs_dir_ok(int fd)
{
	struct stat st;
	int cvs_fd;
	char const cvsdir[] = "CVS";
	char const *cvsfiles[] = {
		"Entries",
		"Repository",
		"Root",
		NULL
	};
	int status;
	int i;
	
	if (fstatat(fd, cvsdir, &st, AT_SYMLINK_NOFOLLOW)) {
		if (errno == ENOENT)
			return 0;
		else {
			diag(LOG_ERR, "can't stat %s: %s", cvsdir,
			     strerror(errno));
			return -1;
		}
	}

	if (!S_ISDIR(st.st_mode))
		return 0;

	cvs_fd = openat(fd, cvsdir, O_RDONLY | O_NONBLOCK | O_DIRECTORY);
	if (cvs_fd == -1) {
		diag(LOG_ERR, "can't open %s: %s", cvsdir, strerror(errno));
		return -1;
	}

	status = 1;

	for (i = 0; cvsfiles[i]; i++) {
		if (fstatat(cvs_fd, cvsfiles[i], &st, AT_SYMLINK_NOFOLLOW)) {
			if (errno == ENOENT)
				status = 0;
			else {
				diag(LOG_ERR, "can't stat %s/%s: %s",
				     cvsdir,
				     cvsfiles[i],
				     strerror(errno));
				status = -1;
			}
			break;
		}

		if (!S_ISREG(st.st_mode)) {
			status = 0;
			break;
		}
	}

	close(cvs_fd);
	return status;
}
