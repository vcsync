/* This file is part of Vcsync
   Copyright (C) 2024 Sergey Poznyakoff

   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Ported from purgedir: https://git.gnu.org.ua/purgedir.git */

#include "vcsync.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>

struct workdir {
	struct workdir *prev, *next;
	struct workdir *parent;
	int fd;
	int keep;
	dev_t st_dev;
	ino_t st_ino;
	char *name;
};

struct file_entry {
	struct workdir *dir;
	dev_t st_dev;
	ino_t st_ino;
	char *name;
};

struct dir_list {
	struct workdir *head, *tail;
	size_t count;
};

#define DIR_LIST_INITIALIZER { NULL, NULL, 0 }

static inline size_t
dir_list_count(struct dir_list *list)
{
	return list->count;
}

static void
dir_list_unlink(struct dir_list *list, struct workdir *ent)
{
	struct workdir *p;

	if ((p = ent->prev) != NULL)
		p->next = ent->next;
	else
		list->head = ent->next;

	if ((p = ent->next) != NULL)
		p->prev = ent->prev;
	else
		list->tail = ent->prev;
	list->count--;

	ent->prev = ent->next = NULL;
}

static void
dir_list_link_head(struct dir_list *list, struct workdir *ent)
{
	ent->next = list->head;
	if (list->head)
		list->head->prev = ent;
	else
		list->tail = ent;
	list->head = ent;
	list->count++;
}

static void
dir_list_link_tail(struct dir_list *list, struct workdir *ent)
{
	ent->prev = list->tail;
	if (list->tail)
		list->tail->next = ent;
	else
		list->head = ent;
	list->tail = ent;
	list->count++;
}

static void workdir_close(struct workdir *dir);
static void workdir_free(struct workdir *dir);

static void
dir_list_free(struct dir_list *list)
{
	struct workdir *dir;
	
	while ((dir = list->head)) {
		dir_list_unlink(list, dir);
		workdir_close(dir);
		workdir_free(dir);
	}
}


typedef struct {
	struct dir_list dir_list_head, dir_list_lru;
	struct file_entry *filetab;
	size_t file_count;
	size_t file_max;
	unsigned errors;
} RMHIER;

static RMHIER *
rmhier_alloc(void)
{
	return grecs_zalloc(sizeof(RMHIER));
}

static void
rmhier_free(RMHIER *rmh)
{
	size_t i;
	
	dir_list_free(&rmh->dir_list_head);
	dir_list_free(&rmh->dir_list_lru);

	for (i = 0; i < rmh->file_count; i++)
		free(rmh->filetab[i].name);
	free(rmh->filetab);
}

#ifndef QRM_DIR_MAX
# define QRM_DIR_MAX 16
#endif
static size_t open_dir_max = QRM_DIR_MAX;

/* Mark DIR and all its parents as being kept. */
static void
workdir_keep(struct workdir *dir)
{
	do {
		dir->keep = 1;
	} while ((dir = dir->parent) != NULL && !dir->keep);
}

static void
workdir_close(struct workdir *dir)
{
	close(dir->fd);
	dir->fd = -1;
}

struct workdir *
workdir_add(RMHIER *rmh, struct workdir *dir, char const *name)
{
	struct workdir *ent;
	char *dir_name;

	if (dir == NULL) {
		dir_name = NULL;
	} else {
		dir_name = dir->name;
	}

	ent = grecs_calloc(1, sizeof(*ent));
	ent->fd = -1;
	ent->name = dir_name ? mkfilename(dir_name, name) : grecs_strdup(name);
	ent->parent = dir;
	dir_list_link_tail(&rmh->dir_list_head, ent);

	return ent;
}

static void
workdir_free(struct workdir *dir)
{
	free(dir->name);
	free(dir);
}

static int
workdir_get(RMHIER *rmh, struct workdir *ent)
{
	if (ent->fd == -1) {
		ent->fd = open(ent->name, O_RDONLY | O_NONBLOCK | O_DIRECTORY);
		if (ent->fd != -1) {
			if (dir_list_count(&rmh->dir_list_lru) == open_dir_max) {
				struct workdir *p = rmh->dir_list_lru.tail;
				workdir_close(p);
				dir_list_unlink(&rmh->dir_list_lru, p);
				dir_list_link_tail(&rmh->dir_list_head, p);
			}
			dir_list_unlink(&rmh->dir_list_head, ent);
			dir_list_link_head(&rmh->dir_list_lru, ent);
		}
	}
	return ent->fd;
}

void *
xgrecs_2nrealloc(void *p, size_t *pn, size_t s)
{
	size_t n = *pn;
	char *newp;

	if (!p) {
		if (!n) {
			/* The approximate size to use for initial small
			   allocation requests, when the invoking code
			   specifies an old size of zero.  64 bytes is the
			   largest "small" request for the GNU C library
			   malloc.  */
			enum { DEFAULT_MXFAST = 64 };

			n = DEFAULT_MXFAST / s;
			n += !n;
		}
	} else {
		/* Set N = ceil (1.5 * N) so that progress is made if N == 1.
		   Check for overflow, so that N * S stays in size_t range.
		   The check is slightly conservative, but an exact check isn't
		   worth the trouble.  */
		if ((size_t) -1 / 3 * 2 / s <= n)
			grecs_alloc_die();
		n += (n + 1) / 2;
	}

	newp = grecs_realloc(p, n * s);
	*pn = n;
	return newp;
}

static void
file_add(RMHIER *rmh, struct workdir *dir, char const *name)
{
	struct file_entry *fent;
	struct stat st;

	if (fstatat(dir->fd, name, &st, AT_SYMLINK_NOFOLLOW) != 0) {
		if (errno == ENOENT)
			return;
		die(EX_FAIL, "can't stat %s/%s: %s", dir->name, name,
		    strerror(errno));
	}

	if (S_ISDIR(st.st_mode)) {
		workdir_add(rmh, dir, name);
		return;
	}

	if (rmh->file_count == rmh->file_max) {
		rmh->filetab = xgrecs_2nrealloc(rmh->filetab,
						&rmh->file_max,
						sizeof(rmh->filetab[0]));
	}
	fent = &rmh->filetab[rmh->file_count++];
	fent->dir = dir;
	fent->st_dev = st.st_dev;
	fent->st_ino = st.st_ino;
	fent->name = grecs_strdup(name);
}

static void
file_add_dir(RMHIER *rmh, struct workdir *dir)
{
	struct file_entry *fent;
	struct stat st;

	if (dir->keep)
		return;

	if (fstat(dir->fd, &st) != 0) {
		if (errno != ENOENT) {
			rmh->errors++;
			diag(LOG_ERR, "can't stat %s: %s", dir->name,
			     strerror(errno));
		}
	}

	if (rmh->file_count == rmh->file_max) {
		rmh->filetab = xgrecs_2nrealloc(rmh->filetab,
						&rmh->file_max,
						sizeof(rmh->filetab[0]));
	}
	fent = &rmh->filetab[rmh->file_count++];
	fent->dir = NULL;
	fent->st_dev = st.st_dev;
	fent->st_ino = st.st_ino;
	fent->name = grecs_strdup(dir->name);
}

static int
file_ent_cmp(const void *a, const void *b)
{
	const struct file_entry *fa = a;
	const struct file_entry *fb = b;

	if (fa->st_dev < fb->st_dev)
		return -1;
	else if (fa->st_dev > fb->st_dev)
		return 1;

	if (fa->st_ino < fb->st_ino)
		return -1;
	else if (fa->st_ino > fb->st_ino)
		return 1;

	return 0;
}

static int
dir_name_cmp(const void *a, const void *b)
{
	const struct file_entry *fa = a;
	const struct file_entry *fb = b;
	ssize_t alen = strlen(fa->name);
	ssize_t blen = strlen(fb->name);
	return blen - alen;
}

static void
file_sort(RMHIER *rmh)
{
	debug(2, ("sorting files"));
	qsort(rmh->filetab, rmh->file_count, sizeof(rmh->filetab[0]),
	      file_ent_cmp);
}

static void
file_remove(RMHIER *rmh, int flag)
{
	size_t i;
	debug(2, ("removing %zu %s\n", rmh->file_count,
		  flag ? "directories" : "files"));
	for (i = 0; i < rmh->file_count; i++) {
		struct file_entry *file = &rmh->filetab[i];
		int dfd;
		char *dir_name;

		if (file->dir) {
			if ((dfd = workdir_get(rmh, file->dir)) == -1) {
				if (errno == ENOENT)
					continue;
				die(EX_FAIL, "can't open directory %s: %s",
				    file->dir->name, strerror(errno));
			}
			dir_name = file->dir->name;
		} else {
			dfd = AT_FDCWD;
			dir_name = NULL;
		}

		if (dir_name)
			debug(2, ("removing %s/%s\n", dir_name, file->name));
		else
			debug(2, ("removing %s\n", file->name));
		
		if (!dry_run && unlinkat(dfd, file->name, flag) &&
		    errno != ENOENT) {
			if (file->dir)
				workdir_keep(file->dir);
			rmh->errors++;
			if (dir_name)
				diag(LOG_ERR, "removing %s/%s: %s",
				     dir_name, file->name, strerror(errno));
			else
				diag(LOG_ERR, "removing %s: %s", file->name,
				     strerror(errno));
		}
	}
}

static void
dir_remove(RMHIER *rmh)
{
	struct workdir *dir;

	rmh->file_count = 0;
	do {
		while ((dir = rmh->dir_list_lru.head) != NULL) {
			dir_list_unlink(&rmh->dir_list_lru, dir);
			file_add_dir(rmh, dir);
			workdir_close(dir);
			workdir_free(dir);
		}
		while ((dir = rmh->dir_list_head.head) != NULL) {
			if (workdir_get(rmh, dir) != -1)
				break;
			else if (errno == ENOENT) {
				dir_list_unlink(&rmh->dir_list_head, dir);
				workdir_free(dir);
			} else
				die(EX_FAIL, "can't open directory %s: %s",
				    dir->name, strerror(errno));
		}
	} while (rmh->dir_list_lru.head);

	qsort(rmh->filetab, rmh->file_count, sizeof(rmh->filetab[0]),
	      dir_name_cmp);
	file_remove(rmh, AT_REMOVEDIR);
}

static void
workdir_collect(RMHIER *rmh, struct workdir *dir)
{
	DIR *dp;
	struct dirent *ent;
	int dfd, fd;

	debug(1, ("collecting directory %s", dir->name));

	if ((dfd = workdir_get(rmh, dir)) == -1) {
		if (errno == ENOENT)
			return;
		die(EX_FAIL, "can't open directory %s: %s", dir->name,
		    strerror(errno));
	}

	fd = dup(dfd);
	if (fd == -1) {
		die(EX_FAIL, "dup failed: %s", strerror(errno));
	}

	dp = fdopendir(fd);
	if (!dp) {
		die(EX_FAIL, "can't open %s: %s", dir->name, strerror(errno));
	}

	while ((ent = readdir(dp)) != NULL) {
		if (strcmp(ent->d_name, ".") == 0 ||
		    strcmp(ent->d_name, "..") == 0)
			continue;
		file_add(rmh, dir, ent->d_name);
	}

	closedir(dp);
}

void
remove_hier(char const *name)
{
	struct stat st;
	struct workdir *dir;
	struct dir_list temp = DIR_LIST_INITIALIZER;
	RMHIER *rmh;

	debug(1, ("removing directory %s", name));

	rmh = rmhier_alloc();
	
	if (fstatat(AT_FDCWD, name, &st, AT_SYMLINK_NOFOLLOW) != 0) {
		die(EX_FAIL, "can't stat %s: %s", name, strerror(errno));
	}

	if (!S_ISDIR(st.st_mode)) {
		die(EX_FAIL, "%s is not a directory", name);
	}

	dir = workdir_add(rmh, NULL, name);
	if (dir == NULL)
		die(EX_FAIL, "%s: no such directory", name);

	do {
		if (workdir_get(rmh, dir) == -1) {
			if (errno == ENOENT) {
				dir_list_unlink(&rmh->dir_list_head, dir);
				workdir_free(dir);
				continue;
			}
			die(EX_FAIL, "can't open directory %s: %s",
			    dir->name, strerror(errno));
		}
		dir_list_unlink(&rmh->dir_list_lru, dir);
		dir_list_link_tail(&temp, dir);
		workdir_collect(rmh, dir);
		workdir_close(dir);
	} while ((dir = rmh->dir_list_head.head) != NULL);

	rmh->dir_list_head = temp;

	file_sort(rmh);
	file_remove(rmh, 0);
	dir_remove(rmh);

	if (rmh->errors)
		die(EX_FAIL,
		    "exiting due to errors occurred while removing"
		    " directory %s", name);
	
	rmhier_free(rmh);
}
