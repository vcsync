/* This file is part of Vcsync
   Copyright (C) 2014-2024 Sergey Poznyakoff
  
   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "vcsync.h"
#include "grecs/locus.h"

const char *fullprogname;
const char *progname;
int log_to_stderr = LOG_DEBUG;

void
setprogname(const char *arg)
{
	char *p;

	fullprogname = arg;
	p = strrchr(arg, '/');
	if (p)
		progname = p + 1;
	else
		progname = arg;
}

void
vcsync_print_grecs_diag(grecs_locus_t const *locus, int err, int errcode,
			 const char *msg)
{
	char *locstr = NULL;
	
	if (locus) {
		size_t size = 0;
		grecs_asprint_locus(&locstr, &size, locus);
	}
  
	if (locstr) {
		if (errcode)
			diag(err ? LOG_ERR : LOG_WARNING, "%s: %s: %s",
			     locstr, msg, strerror(errcode));
		else
			diag(err ? LOG_ERR : LOG_WARNING, "%s: %s",
			     locstr, msg);
		free(locstr);
	} else {
		if (errcode)
			diag(err ? LOG_ERR : LOG_WARNING, "%s: %s", msg,
			     strerror(errcode));
		else
			diag(err ? LOG_ERR : LOG_WARNING, "%s", msg);
	}
}

/* Diagnostic functions */
static const char *
severity(int prio)
{
	switch (prio) {
	case LOG_EMERG:
		return "EMERG";
	case LOG_ALERT:
		return "ALERT";
	case LOG_CRIT:
		return "CRIT";
	case LOG_ERR:
		return "ERROR";
	case LOG_WARNING:
		return "WARNING";
	case LOG_NOTICE:
		return "NOTICE";
	case LOG_INFO:
		return "INFO";
	case LOG_DEBUG:
		return "DEBUG";
	}
	return NULL;
}

void
vdiag(int prio, const char *fmt, va_list ap)
{
	const char *s;
	va_list tmp;
	
	if (log_to_stderr >= prio) {
		fprintf(stderr, "%s: ", progname);
		s = severity(prio);
		if (s)
			fprintf(stderr, "[%s] ", s);
		va_copy(tmp, ap);
		vfprintf(stderr, fmt, tmp);
		fputc('\n', stderr);
		va_end(tmp);
	}

	if (cfg_facility > 0) {
		if (cfg_print_priority && (s = severity(prio)) != NULL) {
			static char *fmtbuf;
			static size_t fmtsize;
			size_t len = strlen(fmt) + strlen(s) + 4;
			char *p;
			
			if (len > fmtsize) {
				fmtbuf = erealloc(fmtbuf, len);
				fmtsize = len;
			}

			p = fmtbuf;
			*p++ = '[';
			while (*s)
				*p++ = *s++;
			*p++ = ']';
			*p++ = ' ';
			while ((*p++ = *fmt++));
			vsyslog(prio, fmtbuf, ap);
		} else			
			vsyslog(prio, fmt, ap);
	}
}

void
diag(int prio, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vdiag(prio, fmt, ap);
	va_end(ap);
}

void
die(int ex, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vdiag(LOG_CRIT, fmt, ap);
	va_end(ap);
	exit(ex);
}
	

void
debugprt(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vdiag(LOG_DEBUG, fmt, ap);
	va_end(ap);
}

