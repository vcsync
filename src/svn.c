/* This file is part of Vcsync
   Copyright (C) 2014-2024 Sergey Poznyakoff
  
   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "vcsync.h"

/* To use SVN mode, add the following to your post-commit hook:

     vcsync $1 $2

   This mode works as follows:

   1. If home directory does not exist, run svn checkout file://$1
   2. If it exists, run "svnlook changed $1 -r $2" and update the
      files it shows.
*/      

char *
svn_proc(int argc, char **argv)
{
	char *directory;
	struct wordsplit ws;
	char *cmd = NULL;
	size_t cmdsize = 0;
		
	if (argc != 2)
		die(EX_USAGE, "expected two arguments: SVNREPO and REV");
	debug(1, ("svn: %s %s", argv[0], argv[1]));
#define svnrepo argv[0]
#define revstr argv[1]
	directory = strrchr(svnrepo, '/');
	if (!directory)
		die(EX_FAIL, "cannot get directory name from %s", svnrepo);
	++directory;
	
	if (!config->vcs_command)
		config->vcs_command = "/usr/bin/svn";

	if (chdir(config->dest_root))
		die(EX_FAIL, "cannot change to %s: %s",
		    config->dest_root, strerror(errno));

	switch (check_output_dir(directory)) {
	case VCSYNC_IGNORE:
		fprintf(stderr,
			"%s is not under VCS control: sentinel file exists\n",
			directory);
		return NULL;

	case VCSYNC_CHECKOUT:
		debug(1, ("checking out %s/%s from SVN", config->dest_root,
			  directory));
		grecs_asprintf(&cmd, &cmdsize,
			       "%s checkout file://%s",
			       config->vcs_command, svnrepo);

		if (wordsplit(cmd, &ws, WRDSF_DEFFLAGS))
			die(EX_FAIL, "wordsplit: %s", wordsplit_strerror(&ws));
		runcmd(ws.ws_wordv, NULL);
		wordsplit_free(&ws);
		register_directory(directory, 1);
		free(cmd);
		return NULL;

	case VCSYNC_UPDATE:{
		vcsync_cmd_stream_t vstr;
		char *p;
		size_t i, xargc, xargalloc;
		char **xargv;
		char *inbuf = NULL;
		size_t insize = 0;
		ssize_t len;

		if (chdir(directory))
			die(EX_FAIL, "cannot change to %s: %s",
			    config->dest_root, directory,
			    strerror(errno));
		
		debug(1, ("getting change list for %s", directory));

		grecs_asprintf(&cmd, &cmdsize,
			       "%s changed %s -r %s",
			       "/usr/bin/svnlook", svnrepo, revstr);

		if (wordsplit(cmd, &ws, WRDSF_DEFFLAGS))
			die(EX_FAIL, "wordsplit: %s", wordsplit_strerror(&ws));
		
		if (runcmd(ws.ws_wordv, &vstr))
			exit(EX_FAIL);
		wordsplit_free(&ws);
		
		xargalloc = 4;
		xargv = ecalloc(xargalloc, sizeof(xargv[0]));
		xargc = 0;
		xargv[xargc++] = config->vcs_command;
		xargv[xargc++] = "update";
		
		while ((len = runcmd_getline(vstr, &inbuf, &insize)) > 0) {
			if (inbuf[len-1] == '\n')
				inbuf[--len] = 0;
			debug(2, ("read: %s", inbuf));

			p = strchr(inbuf, ' ');
			if (!p) {
				diag(LOG_ERR, "unrecognized svnlook line: %s",
				     inbuf);
				continue;
			}
			while (*p && *p == ' ')
				++p;
			if (!*p) {
				diag(LOG_ERR, "unrecognized svnlook line: %s",
				     inbuf);
				continue;
			}	
			if (xargc == xargalloc) {
				size_t n;

				n = xargalloc * 2;
				if (n < xargalloc)
					die(EX_FAIL,
					    "too many arguments collected");
				xargalloc = n;
				xargv = erealloc(xargv,
						 xargalloc*sizeof(xargv[0]));
			}
			xargv[xargc++] = estrdup(p);
			len = strlen(p);
			if (p[len-1] != '/') {
				char *q = strrchr(p, '/');
				if (!q) {
					register_directory(".", 0);
					continue;
				}
				*q = 0;
			}
			register_directory(p, 0);
		}
		
		free(inbuf);
		runcmd_free(vstr);

		debug(1, ("updating %d files in %s/%s from SVN",
			  (unsigned long) xargc,
			  config->dest_root,
			  directory));

		if (xargc == xargalloc) {
			++xargalloc;
			xargv = erealloc(xargv,
					 xargalloc*sizeof(xargv[0]));
		}
		xargv[xargc++] = 0;

		if (runcmd(xargv, NULL))
			exit(EX_FAIL);

		for (i = 2; i < xargc; i++)
			free(xargv[i]);
		free(xargv);
	}
		free(cmd);
		return directory;

	default:
		abort();
	}
	return NULL;
}	

int
svn_dir_ok(int fd)
{
	struct stat st;

	if (fstatat(fd, ".svn", &st, AT_SYMLINK_NOFOLLOW)) {
		if (errno == ENOENT)
			return 0;
		else {
			diag(LOG_ERR, "can't stat .svn: %s", strerror(errno));
			return -1;
		}
	}
	return S_ISDIR(st.st_mode);
}
