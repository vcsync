/* This file is part of Vcsync
   Copyright (C) 2014-2024 Sergey Poznyakoff
  
   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "vcsync.h"

#define MASTER_REF_STR "refs/heads/master"
#define MASTER_REF_LEN (sizeof(MASTER_REF_STR)-1)

static void receive(const char *directory, const char *git_dir, 
		    const char *oldrev, const char *newrev, int clone);

/* Synopsis: to be invoked as git post-receive hook.
   Example usage: cd repo.git/hooks; ln -s /usr/bin/vcsync post-receive
   Arguments: None. Reads data from the stdin.  Each line is:
           REF <sp> OLDREV <sp> NEWREV <lf>
   where <sp> stands for space and <lf> for line feed (newline) characters.
*/
char *
git_proc(int argc, char **argv)
{
	char *git_dir;
	char *directory;
	size_t len;
	int clone = 0;
	struct wordsplit ws;
	int wsflags;
        char *inbuf = NULL;
        size_t insize = 0;
	char *p;
	
	if (argc > 0)
		die(EX_USAGE, "extra arguments");

	git_dir = get_cwd();
	setenv("GIT_DIR", git_dir, 1);
	
	p = strrchr(git_dir, '/');
	if (!p)
		die(EX_FAIL, "cannot get directory name from %s", git_dir);
	++p;
	len = strlen(p);
	if (strncmp(p + len - 4, ".git", 4) == 0)
		len -= 4;

	directory = emalloc(len + 1);
	memcpy(directory, p, len);
	directory[len] = 0;
	
	if (!config->vcs_command)
		config->vcs_command = "/usr/bin/git";
	
	if (chdir(config->dest_root))
		die(EX_FAIL, "cannot change to %s: %s",
		    config->dest_root, strerror(errno));

	switch (check_output_dir(directory)) {
	case VCSYNC_IGNORE:
		fprintf(stderr,
			"%s is not under VCS control: sentinel file exists\n",
			directory);
		return NULL;

	case VCSYNC_CHECKOUT:
		clone = 1;
		break;

	case VCSYNC_UPDATE:
		clone = 0;
		break;

	default:
		abort();
	}
	
	ws.ws_delim = " ";
	wsflags = WRDSF_NOVAR | WRDSF_NOCMD | WRDSF_DELIM;
	while (grecs_getline(&inbuf, &insize, stdin) > 0) {
		size_t len = strlen(inbuf);

		if (len == 0)
			continue;
		if (inbuf[len-1] == '\n')
			inbuf[--len] = 0;
		debug(2, ("read stdin: %s", inbuf));
		if (wordsplit(inbuf, &ws, wsflags))
			die(EX_FAIL, "wordsplit: %s",
			    wordsplit_strerror(&ws));
		wsflags |= WRDSF_REUSE;

		if (ws.ws_wordc != 3)
			die(EX_FAIL, "bad input: %s", inbuf);
		
#define oldrev ws.ws_wordv[0]
#define newrev ws.ws_wordv[1]
#define refname ws.ws_wordv[2]
		
		if (strncmp(refname, MASTER_REF_STR, MASTER_REF_LEN)) {
			diag(LOG_INFO, "git: ignoring update to %s", refname);
			exit(0);
		}

		receive(directory, git_dir, oldrev, newrev, clone);
#undef oldrev
#undef newrev
#undef refname
	}			

	if (wsflags & WRDSF_REUSE)
		wordsplit_free(&ws);

	return clone ? NULL : directory;
}

static void
receive(const char *directory, const char *git_dir,
	const char *oldrev, const char *newrev, int clone)
{
	char *cmd = NULL;
	size_t cmdsize = 0;
	struct wordsplit ws;
	
	if (clone) {
		debug(1, ("cloning %s/%s from GIT", config->dest_root,
			  directory));
		grecs_asprintf(&cmd, &cmdsize,
			       "%s clone %s %s",
			       config->vcs_command, git_dir, directory);
		
		if (wordsplit(cmd, &ws, WRDSF_DEFFLAGS))
			die(EX_FAIL, "wordsplit: %s", wordsplit_strerror(&ws));
		runcmd(ws.ws_wordv, NULL);
		wordsplit_free(&ws);
		register_directory(directory, 1);
	} else {
		vcsync_cmd_stream_t vstr;
		char *inbuf = NULL;
		size_t insize = 0;
		ssize_t len;

		debug(1, ("getting directory change list for %s", directory));
		grecs_asprintf(&cmd, &cmdsize,
			       "%s diff-tree --name-status -r %s..%s",
			       config->vcs_command, oldrev, newrev);
		if (wordsplit(cmd, &ws, WRDSF_DEFFLAGS))
			die(EX_FAIL, "wordsplit: %s", wordsplit_strerror(&ws));

		if (runcmd(ws.ws_wordv, &vstr))
			exit(EX_FAIL);
		wordsplit_free(&ws);

		while ((len = runcmd_getline(vstr, &inbuf, &insize)) > 0) {
                        char *p, *q;

			if (inbuf[len-1] == '\n')
				inbuf[--len] = 0;
			debug(2, ("read: %s", inbuf));
			q = strchr(inbuf, '\t');
			if (!q) {
				diag(LOG_ERR, "unrecognized diff-tree line: %s",
				     inbuf);
				continue;
			}
			++q;
			p = strrchr(q, '/');
			if (!p)
				register_directory(".", 0);
			else {
				*p = 0;
				register_directory(q, 0);
			}
		}
		free(inbuf);

		runcmd_free(vstr);
		
		debug(1, ("git: pull in %s", directory));

		if (chdir(directory))
			die(EX_FAIL, "cannot change to %s: %s",
			    directory, strerror(errno));
		unsetenv("GIT_DIR");
		grecs_asprintf(&cmd, &cmdsize,
			       "%s pull origin master",
			       config->vcs_command);

		if (wordsplit(cmd, &ws, WRDSF_DEFFLAGS))
			die(EX_FAIL, "wordsplit: %s", wordsplit_strerror(&ws));
		
		if (runcmd(ws.ws_wordv, NULL))
			exit(EX_FAIL);
		
		wordsplit_free(&ws);
	}
	free(cmd);
}

int
git_dir_ok(int fd)
{
	struct stat st;

	if (fstatat(fd, ".git", &st, AT_SYMLINK_NOFOLLOW)) {
		if (errno == ENOENT)
			return 0;
		else {
			diag(LOG_ERR, "can't stat .git: %s", strerror(errno));
			return -1;
		}
	}
	return S_ISDIR(st.st_mode);
}
