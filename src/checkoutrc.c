/* This file is part of Vcsync
   Copyright (C) 2014-2024 Sergey Poznyakoff
  
   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "vcsync.h"
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <glob.h>
#include "wordsplit.h"

#define CHECKOUTRC_NAME ".checkoutrc"
#define HTACCESS_FILE ".htaccess"

char *access_file_name;
char *access_file_text;

#define KW_HANDLER  0
#define KW_VERBATIM 1
#define KW_TEXT     2

struct stmt_env {
	char *dirname;
	char *filename;
	unsigned int line;
	size_t argc;
	char **argv;
	char *buf;
	FILE *fp; 
};

struct keyword {
	char *name;
	int type;
	union {
		int (*handler)(struct stmt_env *);
		char *text;
	} v;
};

static int write_htaccess(struct stmt_env *env, const char *fmt, ...);

static FILE *
open_htaccess(struct stmt_env *env)
{
	if (!env->fp) {
		env->fp = fopen(access_file_name, "w");
		if (!env->fp)
			diag(LOG_ERR, "cannot open file %s/%s: %s",
			     env->dirname, access_file_name, strerror(errno));
		if (access_file_text)
			write_htaccess(env, "%s", access_file_text);
	}
	return env->fp;
}

static int
write_htaccess(struct stmt_env *env, const char *fmt, ...)
{
	FILE *fp;
	va_list ap;
	int rc = 0;

	va_start(ap, fmt);

	if (debug_level >= 2) {
                va_list aq;
                va_copy(aq, ap);
                diag(LOG_DEBUG, "%s:%u: write to %s/%s",
                     env->filename, env->line, env->dirname, access_file_name);
                vdiag(LOG_DEBUG, fmt, aq);
                va_end(aq);
	}
	
	if (!dry_run) {
		fp = open_htaccess(env);
		if (!fp)
			return 1;
		rc = vfprintf(env->fp, fmt, ap);
	}
	va_end(ap);
	return rc == -1;
}

static int
checkargs(struct stmt_env *env, int nargs)
{
	if (env->argc != nargs) {
		diag(LOG_ERR, "%s:%u: bad number of arguments",
		     env->filename, env->line);
		return 1;
	}
	return 0;
}

static int
valid_file_name(const char *p)
{
	if (*p == '/')
		return 0;
	for (; *p; p++) {
		if (memcmp(p, "../", 3) == 0)
			return 0;
	}
	return 1;
}

/* Syntax:   link SRC DEST
   Function: create a symbolic link
*/
int
kwf_link(struct stmt_env *env)
{
	if (checkargs(env, 3))
		return 1;

	if (!valid_file_name(env->argv[1])) {
		diag(LOG_ERR, "%s:%u: %s: illegal filename",
		     env->filename, env->line, env->argv[1]);
		return 1;
	}
	if (!valid_file_name(env->argv[2])) {
		diag(LOG_ERR, "%s:%u: %s: illegal filename",
		     env->filename, env->line, env->argv[2]);
		return 1;
	}

	debug(2, ("%s:%u: linking \"%s\" to \"%s\"",
		  env->filename, env->line, env->argv[1], env->argv[2]));
	if (dry_run)
		return 0;
	
	if (unlink(env->argv[2]) && errno != ENOENT) {
		diag(LOG_ERR, "%s:%u: can't unlink %s: %s",
		     env->filename, env->line, env->argv[2],
		     strerror(errno));
		return 1;
	}
	if (symlink(env->argv[1], env->argv[2])) {
		diag(LOG_ERR, "%s:%u: can't link %s to %s: %s",
		     env->filename, env->line, env->argv[1], env->argv[2],
		     strerror(errno));
		return 1;
	}
	return 0;
}

int
kwf_unlink(struct stmt_env *env)
{
	struct stat st;
	char *filename;
	
	if (checkargs(env, 2))
		return 1;

	filename = env->argv[1];
	if (!valid_file_name(filename)) {
		diag(LOG_ERR, "%s:%u: %s: illegal filename",
		     env->filename, env->line, env->argv[1]);
		return 1;
	}

	if (lstat(filename, &st)) {
		if (errno != ENOENT)
			diag(LOG_NOTICE, "unlink: cannot stat %s: %s",
			     filename, strerror(errno));
		return 1;
	}
	if (!S_ISLNK(st.st_mode) &&
	    strcmp(filename + strlen(filename) - strlen(access_file_name),
		   access_file_name)) {
		diag(LOG_ERR, "won't unlink %s", filename);
		return 1;
	}

	if (unlink(filename)) {
		diag(LOG_ERR, "can't unlink %s: %s", 
		     filename, strerror(errno));
		return 1;
	}
	
	return 0;
}

static int
globerrfunc(const char *epath, int eerrno)
{
        diag(LOG_ERR, "%s: %s", epath, strerror(eerrno));
        return 1;
}

static int
do_chmod(struct stmt_env *env, char *file, int mode)
{
	if (!valid_file_name(file)) {
		diag(LOG_ERR,
		     "%s:%u: %s: illegal filename",
		     env->filename, env->line, file);
		return 1;
	}
				
	debug(2, ("%s:%u: chmod %03o \"%s\"",
		  env->filename, env->line, mode, file));
	if (dry_run)
		return 0;
	
	if (chmod(file, mode)) {
		diag(LOG_ERR, "%s:%u: chmod failed: %s",
		     env->filename, env->line, strerror(errno));
		return 1;
	}
	return 0;
}

static int
isglob(const char *s)
{
	for (; *s; s++)
		if (strchr("*?[]", *s))
			return 1;
	return 0;
}

/* Syntax:   chmod MODE FILE [FILE...]
   Function: change mode of a file or files (wildcards allowed).
*/
int
kwf_chmod(struct stmt_env *env)
{
	unsigned long mode;
	char *p;
	int i, j;
	
	if (env->argc < 3) {
		diag(LOG_ERR, "%s:%u: bad number of arguments",
		     env->filename, env->line);
		return 1;
	}

	errno = 0;
	mode = strtoul(env->argv[1], &p, 8);
	if (errno || *p || mode > 0777) {
		diag(LOG_ERR, "%s:%u: %s: bad file mode",
		     env->filename, env->line, env->argv[2]);
		return 1;
	}

	for (i = 2; i < env->argc; i++) {
		if (!valid_file_name(env->argv[i])) {
			diag(LOG_ERR, "%s:%u: %s: illegal filename",
			     env->filename, env->line, env->argv[i]);
			return 1;
		}
		
		if (isglob(env->argv[i])) {
			glob_t g;
			
			switch (glob(env->argv[i], 0, globerrfunc, &g)) {
			case 0:
				break;
			case GLOB_NOSPACE:
				grecs_alloc_die();
			case GLOB_NOMATCH:
				diag(LOG_INFO, "no file matches %s",
				     env->argv[i]);
				return 1;
			default:
				diag(LOG_CRIT, "globbing error");
				return 1;
			}

			for (j = 0; j < g.gl_pathc; j++)
				do_chmod(env, g.gl_pathv[j], mode);
			globfree(&g);
		} else
			do_chmod(env, env->argv[i], mode);
	}	
	return 0;
}


static struct keyword kwtab[MAX_CHECKOUTRC_KW] = {
	{ "link",           KW_HANDLER, { handler: kwf_link } },
	{ "unlink",         KW_HANDLER, { handler: kwf_unlink } },
	{ "chmod",          KW_HANDLER, { handler: kwf_chmod } },
	{ NULL }
};

int
checkoutrc_register_keyword(const char *name, char *text)
{
	struct keyword *kp;

	for (kp = kwtab; kp->name; kp++) {
		if (strcasecmp(kp->name, name) == 0) {
			errno = EEXIST;
			return -1;
		}
	}
	if (kp - kwtab == MAX_CHECKOUTRC_KW) {
		errno = EMFILE;
		return -1;
	}
	kp->name = estrdup(name);
	if (text) {
		size_t len;
		
		kp->type = KW_TEXT;
		kp->v.text = estrdup(text);
		len = strlen(kp->v.text);
		if (kp->v.text[len-1] == '\n')
			kp->v.text[len-1] = 0;
	} else {
		kp->type = KW_VERBATIM;
	}
	return 0;
}	

static struct keyword *
keyword_locate(const char *name)
{
	struct keyword *kp;

	for (kp = kwtab; kp->name; kp++) {
		if (strcasecmp(kp->name, name) == 0)
			return kp;
	}
	return NULL;
}

static int
isbool(char *arg, int *rv)
{
	if (strcasecmp(arg, "on") == 0
	    || strcasecmp(arg, "true") == 0
	    || strcasecmp(arg, "1") == 0) {
		*rv = 1;
		return 1;
	} else if (strcasecmp(arg, "off") == 0
		   || strcasecmp(arg, "false") == 0
		   || strcasecmp(arg, "0") == 0) {
		*rv = 0;
		return 1;
	}
	return 0;
}

static int
keyword_handle(struct keyword *kp, struct stmt_env *env)
{
	int v;
	
	switch (kp->type) {
	case KW_HANDLER:
		return kp->v.handler(env);

	case KW_VERBATIM:
		return write_htaccess(env, "%s", env->buf);

	case KW_TEXT:
		if (checkargs(env, 2))
			break;
		if (isbool(env->argv[1], &v)) {
			if (v)
				return write_htaccess(env, "%s\n", kp->v.text);
		} else {
			diag(LOG_ERR, "%s:%u: expected boolean value, "
			     "but found %s",
			     env->filename, env->line, env->argv[1]);
		}
		break;
		
	default:
		abort();
	}
	return 1;
}

void
checkoutrc(const char *root_dir, const char *dir)
{
	const char *a[5];
	char buf[1024];
	struct wordsplit ws;
	int wsflags;
	struct stmt_env env;
	int err = 0;
	FILE *fp;
	int i;

	if (!access_file_name)
		access_file_name = estrdup(HTACCESS_FILE);
	
	memset(&env, 0, sizeof env);
	env.buf = buf;

	/* First, remove the .htaccess file, if any */
	i = 0;
	a[i++] = config->dest_root;
	if (root_dir)
		a[i++] = root_dir;
	a[i++] = dir;
	a[i++] = access_file_name;
	a[i++] = NULL;
	env.filename = pathcat(a);

	if (access(env.filename, F_OK) == 0 && unlink(env.filename))
		diag(LOG_CRIT, "unable to unlink %s: %s", env.filename,
		     strerror(errno));
	free(env.filename);

	i = 0;
	a[i++] = config->dest_root;
	if (root_dir)
		a[i++] = root_dir;
	a[i++] = dir;
	a[i++] = CHECKOUTRC_NAME;
	a[i++] = NULL;
	env.filename = pathcat(a);

	fp = fopen(env.filename, "r");
	if (!fp) {
		if (errno != ENOENT) {
			diag(LOG_ERR, "cannot open file %s: %s",
			     env.filename, strerror(errno));
		}
		free(env.filename);
		return;
	}

	debug(1, ("processing file %s", env.filename));

	i = 0;
	a[i++] = config->dest_root;
	if (root_dir)
		a[i++] = root_dir;
	a[i++] = dir;
	a[i++] = NULL;
	env.dirname = pathcat(a);

	if (chdir(env.dirname)) {
                diag(LOG_ERR, "cannot change to %s: %s",
                     env.dirname, strerror(errno));
                free(env.dirname);
		free(env.filename);
		fclose(fp);
		return;
        }
	
	env.line = 0;
	wsflags = WRDSF_DEFFLAGS|WRDSF_COMMENT;
	ws.ws_comment = "#";
	while (fgets(buf, sizeof buf, fp)) {
		size_t len;
		struct keyword *kp;
		
		++env.line;

		len = strlen(buf);

		if (len > 0 && buf[len-1] != '\n' && !feof(fp)) {
			int c;
			
			diag(LOG_ERR, "%s:%u: input line too long",
			     env.filename, env.line);
			while ((c = fgetc(fp)) != EOF && c != '\n')
				;
			continue;
		}

		if (wordsplit(buf, &ws, wsflags)) {
			diag(LOG_CRIT, "%s:%u: failed to split line; %s",
			     env.filename, env.line, strerror(errno));
			break;
		}
		wsflags |= WRDSF_REUSE;

		if (ws.ws_wordc == 0)
			continue;
		
		kp = keyword_locate(ws.ws_wordv[0]);
		if (!kp) {
			diag(LOG_ERR, "%s:%u: %s: unrecognized keyword",
			     env.filename, env.line, ws.ws_wordv[0]);
			continue;
		}

		env.argc = ws.ws_wordc;
		env.argv = ws.ws_wordv;
		
		err = keyword_handle(kp, &env);
		if (err)
			break;
	}

	if (wsflags & WRDSF_REUSE)
		wordsplit_free(&ws);

	if (env.fp)
		fclose(env.fp);
	
	free(env.dirname);
	free(env.filename);

	fclose(fp);
}

static char *vcs_dir[] = {
	"CVS",
	".git",
	".svn",
	NULL
};

static int
is_vcs_dir(char const *dir, size_t dirlen)
{
	int i;

	for (i = 0; vcs_dir[i]; i++) {
		size_t len = strlen(vcs_dir[i]);
		if ((len == dirlen
		     || (len < dirlen && dir[dirlen-len-1] == '/'))
		    && memcmp(dir + dirlen - len, vcs_dir[i], len) == 0)
			return 1;
	}
	return 0;	
}

struct directory {
	struct directory *prev, *next;
	int rec;
	char dir[1];
};

static struct directory *dirhead, *dirtail;

void
register_directory0(struct directory *p, const char *dir, int rec)
{
	struct directory *np;
	size_t dirlen;

	dirlen = strlen(dir);
	while (dirlen && dir[dirlen-1] == '/')
		dirlen--;

	if (is_vcs_dir(dir, dirlen))
		return;
	
	for (; p; p = p->next) {
		if (dirlen == strlen(p->dir)) {
			int rc = memcmp(p->dir, dir, dirlen);
			if (rc == 0)
				return;
			if (rc > 0)
				break;
		}
	}

	debug(5, ("registering directory %s (recursive=%d)", dir, rec));
	np = emalloc(sizeof(*np) + dirlen);
	memcpy(np->dir, dir, dirlen);
	np->dir[dirlen] = 0;
	np->rec = rec;
	np->next = p;
	if (p) {
		np->prev = p->prev;
		if (p->prev)
			p->prev->next = np;
		else
			dirhead = np;
		p->prev = np;
	} else {
		np->prev = dirtail;
		if (dirtail)
			dirtail->next = np;
		else
			dirhead = np;
		dirtail = np;
	}
}

void
register_directory(const char *dir, int rec)
{
	register_directory0(dirhead, dir, rec);
}

void
printdirs()
{
	struct directory *p;
	
	for (p = dirhead; p; p = p->next)
		printf("%s\n", p->dir);
}

int
post_socket_send(const struct vcsync_sockaddr *sockaddr, const char *root_dir)
{
	int domain;
	int fd;
	FILE *fp;
	char buf[128];
	char *p;

	if (sockaddr->len == 0)
		return 0;
	
	switch (sockaddr->sa->sa_family) {
	case AF_UNIX:
		domain = PF_UNIX;
		break;

	case AF_INET:
		domain = PF_INET;
		break;

	default:
		/* should not happen */
		abort();
	}
	
	fd = socket(domain, SOCK_STREAM, 0);
	if (fd == -1) {
		diag(LOG_ERR, "socket: %s", strerror(errno));
		return 1;
	}
	if (connect(fd, sockaddr->sa, sockaddr->len)) {
		diag(LOG_ERR, "connect: %s", strerror(errno));
		return 1;
	}

	fp = fdopen(fd, "a+");
	/* Communication takes place in accordance with TCPMUX
	   protocol (RFC 1078).  The rule tag is used as service
	   name. */
	fprintf(fp, "%s\r\n",
		config->post_sync_tag ? config->post_sync_tag : "vcsync");
	p = fgets(buf, sizeof(buf), fp);
	if (!p)
		diag(LOG_ERR, "no response from TCPMUX");
	else if (*p == '+') {
		struct directory *p;

		fprintf(fp, "%s", config->dest_root);
		if (root_dir)
			fprintf(fp, "/%s", root_dir);
		fprintf(fp, "\r\n");
		for (p = dirhead; p; p = p->next)
			fprintf(fp, "%s\r\n", p->dir);
	} else
		diag(LOG_ERR, "TCPMUX returned %s", p);
	fflush(fp);
	fclose(fp);
	return 0;
}

void
flush_checkoutrc_files(const char *root_dir)
{
	struct directory *p;
	char *namebuf = NULL;
	size_t namelen = 0;
	size_t preflen;
	struct stat st;

	if (!dirhead)
		return;

	if (chdir(config->dest_root))
		die(EX_FAIL, "%s: %s", config->dest_root, strerror(errno));
	if (root_dir && chdir(config->dest_root))
		die(EX_FAIL, "%s/%s: %s", config->dest_root, root_dir,
		    strerror(errno));
	
	/* Pass 1: resolve recursion */
	for (p = dirhead; p; p = p->next) {
		if (p->rec) {
			DIR *d;
			struct dirent *ent;
			size_t len;

			preflen = strlen(p->dir);

			if (!namebuf) {
				namelen = preflen * 2 + 1;
				namebuf = emalloc(namelen);
			} else {
				len = preflen * 2 + 1;
				
				while (namelen <= len) {
					size_t n = namelen * 2;
					if (n < namelen)
						die(EX_FAIL,
						    "not enough memory");
					namelen = n;
					namebuf = erealloc(namebuf, namelen);
				}
			}
			strcpy(namebuf, p->dir);

			debug(5, ("scanning %s", namebuf));
			d = opendir(namebuf);
			if (!d) {
				diag(LOG_ERR, "can't open directory %s: %s",
				     namebuf, strerror(errno));
				continue;
			}
			namebuf[preflen++] = '/';
			
			while ((ent = readdir(d))) {
				const char *name = ent->d_name;
				
				/* Skip "", ".", and "..".  "" is returned
				   by at least one buggy implementation:
				   Solaris 2.4 readdir on NFS file systems.  */
				if (!(name[name[0] != '.'
					    ? 0
					    : name[1] != '.' ? 1 : 2] != '\0'))
					continue;
			
				len = preflen + strlen(name);
				while (namelen <= len) {
					size_t n = namelen * 2;
					if (n < namelen)
						die(EX_FAIL,
						    "not enough memory");
					namelen = n;
					namebuf = erealloc(namebuf, namelen);
				}
				strcpy(namebuf + preflen, name);
				
				if (stat(namebuf, &st)) {
					diag(LOG_ERR, "can't stat %s: %s",
					     namebuf, strerror(errno));
					continue;
				}

				if (S_ISDIR(st.st_mode)) {
					register_directory0(p, namebuf, 1);
				}
			}
			closedir(d);
		}
	}
	free(namebuf);
	
	/* Pass 2: process checkoutrc files */
	for (p = dirhead; p; p = p->next)
		checkoutrc(root_dir, p->dir);

	/* Pass 3: Process notifications */
	post_socket_send(&config->post_sync_socket, root_dir);
}

int
have_checkoutrc()
{
	return !!dirhead;
}
