/* This file is part of Vcsync
   Copyright (C) 2014-2024 Sergey Poznyakoff
  
   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "vcsync.h"

int cfg_facility = LOG_USER;
int cfg_print_priority;
char *cfg_tag;
struct vcsync_config default_config;
static struct vcsync_config *config_head, *config_tail;
struct vcsync_config *config;
char const *config_name;

static int
cb_vcs_type(enum grecs_callback_command cmd, grecs_node_t *node,
	    void *varptr, void *cb_data)
{
	grecs_locus_t *locus = &node->locus;
	grecs_value_t *value = node->v.value;
	struct vcsync_config *conf = varptr;

	grecs_assert_scalar_stmt(locus, cmd);
	if (grecs_assert_value_type(value, GRECS_TYPE_STRING, &value->locus))
		return 1;
	conf->vcs_type = estrdup(value->v.string);
	conf->vcs = vcs_defn_get(value->v.string);
	if (!conf->vcs)
		grecs_error(locus, 0, "unknown VCS");
	return 0;
}

static int
cb_define(enum grecs_callback_command cmd, grecs_node_t *node,
	  void *varptr, void *cb_data)
{
	grecs_locus_t *locus = &node->locus;
	grecs_value_t *value = node->v.value;
	int rc;
	
	grecs_assert_scalar_stmt(locus, cmd);
	switch (value->type) {
	case GRECS_TYPE_STRING:
		rc = checkoutrc_register_keyword(value->v.string, NULL);
		break;

	case GRECS_TYPE_ARRAY:
		switch (value->v.arg.c) {
		case 1:
			if (grecs_assert_value_type(value->v.arg.v[0],
						    GRECS_TYPE_STRING,
				                    &value->v.arg.v[0]->locus))
				return 1;
			rc = checkoutrc_register_keyword
				(value->v.arg.v[0]->v.string, NULL);
			break;
		case 2:
			if (grecs_assert_value_type(value->v.arg.v[0],
						    GRECS_TYPE_STRING,
				                    &value->v.arg.v[0]->locus))
				return 1;
			if (grecs_assert_value_type(value->v.arg.v[1],
						    GRECS_TYPE_STRING,
				                    &value->v.arg.v[1]->locus))
				return 1;
			rc = checkoutrc_register_keyword
				(value->v.arg.v[0]->v.string,
				 value->v.arg.v[1]->v.string);
			break;
		default:
			grecs_error(locus, 0, "too many arguments");
			return 1;
		}
		break;

	default:
		return 1;
	}

	if (rc) {
		switch (errno) {
		case EMFILE:
			grecs_error(locus, 0, "too many keywords defined");
			break;
		case EEXIST:
			grecs_error(locus, 0, "keyword already defined");
			break;
		}
	}
	
	return rc;
}

int
get_facility(const char *arg, int *retval)
{
	static struct transtab kwfac[] = {
		{ "user",    LOG_USER },
		{ "daemon",  LOG_DAEMON },
		{ "auth",    LOG_AUTH },
		{ "authpriv",LOG_AUTHPRIV },
		{ "mail",    LOG_MAIL },
		{ "cron",    LOG_CRON },
		{ "local0",  LOG_LOCAL0 },
		{ "local1",  LOG_LOCAL1 },
		{ "local2",  LOG_LOCAL2 },
		{ "local3",  LOG_LOCAL3 },
		{ "local4",  LOG_LOCAL4 },
		{ "local5",  LOG_LOCAL5 },
		{ "local6",  LOG_LOCAL6 },
		{ "local7",  LOG_LOCAL7 },
		{ NULL }
	};

	int f;
	char *p;

	errno = 0;
	f = strtoul(arg, &p, 0);
	if (!(*p == 0 && errno == 0) && trans_strtotok(kwfac, arg, &f)) 
		return -1;
	*retval = f;
	return 0;
}

int
get_priority(const char *arg, int *retval)
{
	static struct transtab kwpri[] = {
		{ "emerg", LOG_EMERG },
		{ "alert", LOG_ALERT },
		{ "crit", LOG_CRIT },
		{ "err", LOG_ERR },
		{ "warning", LOG_WARNING },
		{ "notice", LOG_NOTICE },
		{ "info", LOG_INFO },
		{ "debug", LOG_DEBUG },
		{ NULL }
	};

	int f;
	char *p;

	errno = 0;
	f = strtoul(arg, &p, 0);
	if (!(*p == 0 && errno == 0) && trans_strtotok(kwpri, arg, &f))
		return -1;
	*retval = f;
	return 0;
}

static int
cb_syslog_facility(enum grecs_callback_command cmd, grecs_node_t *node,
		   void *varptr, void *cb_data)
{
	grecs_locus_t *locus = &node->locus;
	grecs_value_t *value = node->v.value;

	grecs_assert_scalar_stmt(locus, cmd);
	if (grecs_assert_value_type(value, GRECS_TYPE_STRING, &value->locus))
		return 1;

	if (strcmp(value->v.string, "none") == 0)
		*(int*)varptr = 0;
	else if (get_facility(value->v.string, varptr))
		grecs_error(&value->locus, 0,
			    "Unknown syslog facility `%s'",
			    value->v.string);
	return 0;
}

static struct grecs_keyword syslog_kw[] = {
	{ "facility",
	  "name",
	  "Set syslog facility. Arg is one of the following: none, user, daemon, "
	  "auth, authpriv, mail, cron, local0 through local7 "
	  "(case-insensitive), or a facility number.",
	  grecs_type_string, GRECS_DFLT,
	  &cfg_facility, 0, cb_syslog_facility },
	{ "tag", "string", "Tag syslog messages with this string",
	  grecs_type_string, GRECS_DFLT,
	  &cfg_tag },
	{ "print-priority", "arg",
	  "Prefix each message with its priority",
	  grecs_type_bool, GRECS_DFLT,
	  &cfg_print_priority },
	{ NULL },
};

struct socket_family {
	char *name;
	size_t len;
	int family;
};

static struct socket_family socket_family[] = {
#define DEF(s, f) { #s, sizeof(#s)-1, f }
	DEF(inet, AF_INET),
	DEF(unix, AF_UNIX),
	DEF(local, AF_UNIX),
	{ NULL }
#undef DEF
};

static struct socket_family *
find_family(const char *s, size_t len)
{
	struct socket_family *fp;
	for (fp = socket_family; fp->name; fp++)
		if (len == fp->len)
			return fp;
	return NULL;
}

static char *
copy_part(const char *cstr, const char *p)
{
	size_t len = p - cstr;
	char *buf = emalloc(len + 1);
	memcpy(buf, cstr, len);
	buf[len] = 0;
	return buf;
}

static void
parse_conn(const char *cstr, char **pport, char **ppath)
{
	const char *p = strchr (cstr, ':');

	if (!p) {
		*pport = NULL;
		*ppath = strdup(cstr);
	} else {
		*ppath = copy_part(cstr, p);
		*pport = estrdup(p + 1);
	}
}

static int
parse_url(grecs_locus_t *locus, const char *cstr, 
	  int *pfamily, char **pport, char **ppath)
{
	const char *p;
	
	p = strchr(cstr, ':');
	if (p) {
		struct socket_family *fp = find_family(cstr, p - cstr);

		if (!fp) {
			grecs_error(locus, 0, "unknown address family");
			return 1;
		}

		*pfamily = fp->family;
		
		cstr = p + 1;
		if (cstr[0] == '/') {
			if (cstr[1] == '/') {
				parse_conn(cstr + 2, pport, ppath);
			} else if (*pfamily == AF_UNIX) {
				*pport = NULL;
				*ppath = estrdup(cstr);
			}
		} else {
			grecs_error(locus, 0, "malformed URL");
			return 1;
		}
	} else {
		*pfamily = AF_UNIX;
		*pport = NULL;
		*ppath = estrdup(cstr);
	}
	return 0;
}

static int
make_socket(grecs_locus_t *locus, int family,
	    char *port, char *path, struct vcsync_sockaddr *pa)
{
	union {
		struct sockaddr sa;
		struct sockaddr_un s_un;
		struct sockaddr_in s_in;
	} addr;
	socklen_t socklen;
	short pnum;
	long num;
	char *p;
	
	switch (family) {
	case AF_UNIX:
		if (port) {
			grecs_error(locus, 0,
				    "port is meaningless for UNIX sockets");
			return 1;
		}
		if (strlen(path) > sizeof addr.s_un.sun_path) {
			errno = EINVAL;
			grecs_error(locus, 0, "UNIX socket name too long");
			return 1;
		}
		
		addr.sa.sa_family = PF_UNIX;
		socklen = sizeof(addr.s_un);
		strcpy(addr.s_un.sun_path, path);
		break;

	case AF_INET:
		addr.sa.sa_family = PF_INET;
		socklen = sizeof(addr.s_in);

		num = pnum = strtol(port, &p, 0);
		if (*p == 0) {
			if (num != pnum) {
				grecs_error(locus, 0, "bad port number");
				return -1;
			}
			pnum = htons(pnum);
		} else {
			struct servent *sp = getservbyname(port, "tcp");
			if (!sp) {
				grecs_error(locus, 0, "unknown service name");
				return -1;
			}
			pnum = sp->s_port;
		}

		if (!path)
			addr.s_in.sin_addr.s_addr = INADDR_ANY;
		else {
			struct hostent *hp = gethostbyname(path);
			if (!hp) {
				grecs_error(locus, 0,
					    "unknown host name %s",
					    path);
				return 1;
			}
			addr.sa.sa_family = hp->h_addrtype;
			switch (hp->h_addrtype) {
			case AF_INET:
				memmove(&addr.s_in.sin_addr, hp->h_addr, 4);
				addr.s_in.sin_port = pnum;
				break;

			default:
				grecs_error(locus, 0,
					    "unsupported address family");
				return 1;
			}
		}
		break;

	default:
		grecs_error(locus, 0, "unsupported address family");
		return 1;
	}
	pa->len = socklen;
	pa->sa = emalloc(socklen);
	memcpy(pa->sa, &addr.sa, socklen);
	return 0;
}

static int
cb_sockaddr(enum grecs_callback_command cmd, grecs_node_t *node,
	    void *varptr, void *cb_data)
{
	grecs_locus_t *locus = &node->locus;
	grecs_value_t *value = node->v.value;
	int family;
	char *path;
	char *port;
	
	grecs_assert_scalar_stmt(locus, cmd);
	if (grecs_assert_value_type(value, GRECS_TYPE_STRING, &value->locus))
		return 1;
	
	if (parse_url(&value->locus, value->v.string, 
		      &family, &port, &path))
		return 1;

	return make_socket(&value->locus, family, port ? port : "tcpmux", path,
			   (struct vcsync_sockaddr *)varptr);
}


static int
cb_config(enum grecs_callback_command cmd, grecs_node_t *node,
	  void *varptr, void *cb_data)
{
	struct vcsync_config *cp, **pcp = cb_data;
	
	switch (cmd) {
	case grecs_callback_section_begin:
		if (grecs_assert_value_type(node->v.value, GRECS_TYPE_STRING,
					    &node->locus))
			return 1;
		cp = ecalloc(1, sizeof(*cp));
		cp->name = estrdup(node->v.value->v.string);
		*pcp = cp;
		break;

	case grecs_callback_section_end:
		cp = *pcp;
		if (config_tail)
			config_tail->next = cp;
		else
			config_head = cp;
		config_tail = cp;
		break;

	case grecs_callback_set_value:
		grecs_error(&node->locus, 0, "invalid use of block statement");
	}
	return 0;
}		
		
static int
cb_foreign_dir_mode(enum grecs_callback_command cmd, grecs_node_t *node,
	    void *varptr, void *cb_data)
{
	grecs_locus_t *locus = &node->locus;
	grecs_value_t *value = node->v.value;
	static struct transtab kwfac[] = {
		{ "error", FOREIGN_DIR_ERROR },
		{ "remove", FOREIGN_DIR_REMOVE },
		{ "rename", FOREIGN_DIR_RENAME },
		{ "archive", FOREIGN_DIR_ARCHIVE },
		{ NULL }
	};
	
	grecs_assert_scalar_stmt(locus, cmd);
	if (grecs_assert_value_type(value, GRECS_TYPE_STRING, &value->locus))
		return 1;
	return trans_strtotok(kwfac, value->v.string, varptr);
}

static struct grecs_keyword vcsync_config_kw[] = {
	{ "debug", "level", "Set debug level",
	  grecs_type_int, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, debug_level) },
	{ "vcs-type", "arg: cvs|svn|git", "Set the type of VCS to use",
	  grecs_type_string, GRECS_DFLT,
	  NULL, 0, cb_vcs_type },
	{ "vcs-command", "command", "Set VCS command line",
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, vcs_command) },
	{ "destination-root",
	  "dir", "Pathname of the of the WWW root directory",
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, dest_root) },
	{ "timeout", "seconds", "Command execution timeout",
	  grecs_type_uint, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, timeout) },
	{ "detach", NULL,
	  "Detach from the controlling terminal before synchronizing",
	  grecs_type_bool, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, detach) },
	{ "sleep", "seconds", "Seconds to sleep before synchronizing",
	  grecs_type_uint, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, sleep_time) },
	{ "message", "text", "Message to display before synchronizing",
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, message) },
	{ "post-sync-socket", "URL",
	  "Send info to this socket after synchronizing",
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, post_sync_socket), cb_sockaddr },
	{ "post-sync-tag", "text", "Tag for post-sync communication",
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, post_sync_tag) },
	{ "sentinel", "file", "Name of the VCS sentinel file.",
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, sentinel_file) },
	{ "foreign-dir-mode", "mode", "How to handle foreign VCS directories",
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, foreign_dir_mode),
	  cb_foreign_dir_mode },
	{ "archive-command", "command", "Command to use to archive a directory.",
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct vcsync_config, archive_command) },
	{ NULL }
};

static struct grecs_keyword vcsync_kw[] = {
	{ "debug", "level", "Set debug level",
	  grecs_type_int, GRECS_DFLT,
	  &default_config.debug_level },
	{ "vcs-type", "arg: cvs|svn|git", "Set the type of VCS to use",
	  grecs_type_string, GRECS_DFLT,
	  &default_config, 0, cb_vcs_type },
	{ "vcs-command", "command", "Set VCS command line",
	  grecs_type_string, GRECS_DFLT,
	  &default_config.vcs_command },
	{ "destination-root",
	  "dir", "Pathname of the of the WWW root directory",
	  grecs_type_string, GRECS_DFLT,
	  &default_config.dest_root },
	{ "timeout", "seconds", "Command execution timeout",
	  grecs_type_uint, GRECS_DFLT,
	  &default_config.timeout },
	{ "detach", NULL,
	  "Detach from the controlling terminal before synchronizing",
	  grecs_type_bool, GRECS_DFLT,
	  &default_config.detach },
	{ "sleep", "seconds", "Seconds to sleep before synchronizing",
	  grecs_type_uint, GRECS_DFLT,
	  &default_config.sleep_time },
	{ "message", "text", "Message to display before synchronizing",
	  grecs_type_string, GRECS_DFLT,
	  &default_config.message },
	{ "post-sync-socket", "URL",
	  "Send info to this socket after synchronizing",
	  grecs_type_string, GRECS_DFLT,
	  &default_config.post_sync_socket, 0, cb_sockaddr },
	{ "post-sync-tag", "text", "Tag for post-sync communication",
	  grecs_type_string, GRECS_DFLT,
	  &default_config.post_sync_tag },
	{ "sentinel", "file", "Name of the VCS sentinel file.",
	  grecs_type_string, GRECS_DFLT,
	  &default_config.sentinel_file },

	{ "foreign-dir-mode", "mode", "How to handle foreign VCS directories",
	  grecs_type_string, GRECS_DFLT,
	  &default_config.foreign_dir_mode, 0,
	  cb_foreign_dir_mode },
	{ "archive-command", "command", "Command to use to archive a directory.",
	  grecs_type_string, GRECS_DFLT,
	  &default_config.archive_command },

	{ "config", "name: string", "Define a named configuration",
	  grecs_type_section, GRECS_DFLT, NULL, 0, cb_config, NULL,
	  vcsync_config_kw },

	{ "define", "<kw: string> [<text: string>]",
	  "Define checkoutrc keyword",
	  grecs_type_string, GRECS_DFLT,
	  NULL, 0, cb_define },

	{ "access-file-name", "name", "Name of the access file",
	  grecs_type_string, GRECS_DFLT,
	  &access_file_name },
	{ "access-file-text", "text",
	  "Static text to add to each created access file",
	  grecs_type_string, GRECS_DFLT,
	  &access_file_text },
	
	{ "syslog", NULL, "Configure syslog logging",
	  grecs_type_section, GRECS_DFLT, NULL, 0, NULL, NULL, syslog_kw },
	
	{ NULL }	
};

void
config_help()
{
	static char docstring[] =
		"Configuration file structure for vcsync.\n"
		"For more information, see info vcsync.";
	grecs_print_docstring(docstring, 0, stdout);
	grecs_print_statement_array(vcsync_kw, 1, 0, stdout);
}

void
config_init()
{
        grecs_log_to_stderr = 1;
	grecs_print_diag_fun = vcsync_print_grecs_diag;
}

static struct vcsync_config *
vcsync_config_lookup_name(char const *name)
{
	struct vcsync_config *cp;

	for (cp = config_head; cp; cp = cp->next) {
		if (strcmp(cp->name, name) == 0)
			break;
	}
	return cp;
}

static struct vcsync_config *
vcsync_config_lookup_type(char const *type)
{
	struct vcsync_config *cp;

	for (cp = config_head; cp; cp = cp->next) {
		if (strcmp(cp->vcs_type, type) == 0)
			break;
	}
	return cp;
}

void
config_finish(struct grecs_node *tree)
{
	if (tree && grecs_tree_process(tree, vcsync_kw))
		exit(EX_CONFIG);

	if (config_name) {
		config = vcsync_config_lookup_name(config_name);
		if (!config)
			die(EX_USAGE, "configuration \"%s\" not defined",
			    config_name);
	} else if (default_config.vcs_type && !vcs_type) {
		default_config.vcs = vcs_defn_get(default_config.vcs_type);
		if (!default_config.vcs)
			die(EX_USAGE, "unknown VCS: %s",
			    default_config.vcs_type);
		config = &default_config;
	} else {
		if (!vcs_type) {
			if ((vcs_type = guess_vcs_type())) {
				config = vcsync_config_lookup_type(vcs_type);
			} else
				vcs_type = DEFAULT_VCS;
		}
		if (!config) {
			config = vcsync_config_lookup_type(vcs_type);
			if (!config) {
				default_config.vcs_type = (char*)vcs_type;
				default_config.vcs = vcs_defn_get(vcs_type);
				if (!default_config.vcs)
					die(EX_USAGE, "unknown VCS: %s",
					    vcs_type);
				config = &default_config;
			}
		}
	}

	if (!debug_level)
		debug_level = config->debug_level;
	if (!cfg_tag)
		cfg_tag = (char*) progname;

	if (destination_root)
		config->dest_root = destination_root;
	else if (!config->dest_root) {
		if (!config->name)
			diag(LOG_ERR,
			    "no destination directory (using default configuration)");
		else
			die(EX_CONFIG,
			    "no destination directory (using named configuration %s)", config->name);
	}

	if (sleep_option)
		config->sleep_time = sleep_option;
	if (detach_option)
		config->detach = detach_option;

	if (config->foreign_dir_mode == FOREIGN_DIR_ARCHIVE &&
	    config->archive_command == NULL) {
		die(EX_CONFIG,
		    "foreign-dir-mode is set to \"archive\", but no archive-command is defined");
	}
}
