/* This file is part of Vcsync
   Copyright (C) 2014-2024 Sergey Poznyakoff

   Vcsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vcsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Vcsync.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "vcsync.h"
#include "runcap.h"

struct vcsync_cmd_stream {
	struct runcap rc;
	char *argv[1];
};

static void
linemon_stdout(const char *buf, size_t size, void *closure)
{
	if (size) {
		if (buf[size-1] == '\n')
			--size;
		diag(LOG_INFO, "%*.*s", (int)size, (int)size, buf);
	}
}

static void
linemon_stderr(const char *buf, size_t size, void *closure)
{
	if (size) {
		if (buf[size-1] == '\n')
			--size;
		diag(LOG_ERR, "%*.*s", (int)size, (int)size, buf);
	}
}

int
runcmd(char **argv, vcsync_cmd_stream_t *pstr)
{
	struct vcsync_cmd_stream *str;
	size_t i, argc;
	size_t strsize, argsize;
	char *p;
	int rcf;
	
	if (debug_level >= 1) {
		struct grecs_txtacc *acc = grecs_txtacc_create();

		grecs_txtacc_grow_string(acc, argv[0]);
		for (i = 1; argv[i]; i++) {
			grecs_txtacc_grow_char(acc, ' ');
			grecs_txtacc_grow_string_escape(acc, argv[i]);
		}
		grecs_txtacc_grow_char(acc, 0);

		debugprt("starting %s", grecs_txtacc_finish(acc, 0));
		grecs_txtacc_free(acc);
	}

	if (dry_run)
		return 0;

	for (argc = 0, argsize = 0; argv[argc]; argc++)
		argsize += strlen(argv[argc]) + 1;

	strsize = sizeof(*str) + argc * sizeof(argv[0]);
	str = emalloc(strsize + argsize);
	p = (char*)str + strsize;
	for (i = 0; i < argc; i++) {
		size_t len = strlen(argv[i]) + 1;
		str->argv[i] = p;
		memcpy(p, argv[i], len);
		p += len;
	}
	str->argv[i] = NULL;

	str->rc.rc_argv = str->argv;
	str->rc.rc_timeout = config->timeout;
	str->rc.rc_cap[RUNCAP_STDOUT].sc_linemon = linemon_stdout;
	str->rc.rc_cap[RUNCAP_STDERR].sc_linemon = linemon_stderr;

	rcf = RCF_TIMEOUT
		| RCF_STDOUT_LINEMON
		| RCF_STDERR_LINEMON
		| RCF_STDERR_NOCAP;
	if (!pstr)
		rcf |= RCF_STDOUT_NOCAP;
	
	if (runcap(&str->rc, rcf)) {
		diag(LOG_ERR, "runcap: %d", strerror(errno));
		free(str);
		exit(EX_FAIL);
	}

	if (pstr) {
		*pstr = str;
		return 0;
	}
	return runcmd_free(str);
}

int
runcmd_free(vcsync_cmd_stream_t str)
{
	int res = 1;
	int status = str->rc.rc_status;

	if (WIFEXITED(status)) {
		if (WEXITSTATUS(status) == 0) {
			debug(1, ("%s terminated successfully",
				  str->rc.rc_argv[0]));
			res = 0;
		} else
			diag(LOG_NOTICE,
			     "%s exited with status %d",
			     str->rc.rc_argv[0], WEXITSTATUS(status));
	} else if (WIFSIGNALED(status)) {
		diag(LOG_NOTICE,
		     "%s terminated on signal %d",
		     str->rc.rc_argv[0], WTERMSIG(status));
	} else if (WIFSTOPPED(status))
		diag(LOG_NOTICE, "%s stopped", str->rc.rc_argv[0]);
	else {
		diag(LOG_CRIT, "%s terminated with unrecognized status: %d",
		     status);
	}
	runcap_free(&str->rc);
	free(str);
	return res;
}

ssize_t
runcmd_getline(vcsync_cmd_stream_t str, char **pstr, size_t *psize)
{
	return runcap_getline(&str->rc, RUNCAP_STDOUT, pstr, psize);
}
